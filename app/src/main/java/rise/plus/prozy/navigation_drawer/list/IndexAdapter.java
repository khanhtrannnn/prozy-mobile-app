package rise.plus.prozy.navigation_drawer.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import rise.plus.prozy.R;

public class IndexAdapter extends RecyclerView.Adapter<IndexAdapter.ViewHolder> {

    private List<index> mIndices;
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageButton mImageButton;
        public TextView mName;
        public TextView mDistance;
        public TextView mPrice;
        public RatingBar mRating;

        public ViewHolder(View view) {
            super(view);
            mImageButton = (ImageButton) view.findViewById(R.id.imageButton);
            mName = (TextView) view.findViewById(R.id.name);
            mDistance = (TextView) view.findViewById(R.id.tvDistance);
            mPrice = (TextView) view.findViewById(R.id.tvPrice);
            mRating = (RatingBar) view.findViewById(R.id.product_rating);
        }
    }

    public IndexAdapter(Context context, List<index> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_favorite, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        index index = mIndices.get(position);

        holder.mName.setText(index.getName());
        holder.mPrice.setText(index.getPrice()+"");
        holder.mDistance.setText(index.getDistance()+"");
        holder.mRating.setRating(index.getRating());
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}