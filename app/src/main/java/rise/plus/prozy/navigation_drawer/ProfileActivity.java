package rise.plus.prozy.navigation_drawer;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import rise.plus.prozy.BaseActivity;
import rise.plus.prozy.R;

import rise.plus.prozy.services.UserGet;
import rise.plus.prozy.services.async_response.UserResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.host;

public class ProfileActivity extends BaseActivity implements UserResponse, View.OnClickListener {

    private boolean fabButton = true;
    private LinearLayout edtLayout, showLayout;
    private TextView tvName, tvLastName, tvTel, tvEmail, tvNameStatic, tvLastNameStatic, tvTelStatic, tvEmailStatic, tvFullName;
    private EditText editTextName, editTextLastName, editTextTel, editTextEmail;
    private FloatingActionButton fab;
    private ImageView ivBack, ivAvatar;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        navigationView();
        setSupportActionBar(toolbar);
        showToolBar(toolbar, this);
        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_image));
        textStyle();

        mToggle = new ActionBarDrawerToggle(ProfileActivity.this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();

        new UserGet(this, "/users/" + UID + ".json").execute();

    }

    protected void init() {
        tvName = (TextView) findViewById(R.id.name);
        tvLastName = (TextView) findViewById(R.id.lastname);
        tvTel = (TextView) findViewById(R.id.tel);
        tvEmail = (TextView) findViewById(R.id.email);
        ivAvatar = (ImageView) findViewById(R.id.avatar);
        tvFullName = (TextView) findViewById(R.id.fullname);
        tvNameStatic = (TextView) findViewById(R.id.name_static);
        tvLastNameStatic = (TextView) findViewById(R.id.lastname_static);
        tvTelStatic = (TextView) findViewById(R.id.tel_static);
        tvEmailStatic = (TextView) findViewById(R.id.email_static);
        editTextName = (EditText) findViewById(R.id.edtName);
        editTextLastName = (EditText) findViewById(R.id.edtLastName);
        editTextTel = (EditText) findViewById(R.id.edtTel);
        editTextEmail = (EditText) findViewById(R.id.edtEmail);
        edtLayout = (LinearLayout) findViewById(R.id.edit_profile);
        showLayout = (LinearLayout) findViewById(R.id.tv_profile);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    protected void textStyle() {
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Diodrum Medium.otf");
        tvName.setTypeface(typeface);
        tvLastName.setTypeface(typeface);
        tvEmail.setTypeface(typeface);
        tvTel.setTypeface(typeface);

        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Diodrum Semibold.otf");
        tvNameStatic.setTypeface(typeface2);
        tvLastNameStatic.setTypeface(typeface2);
        tvEmailStatic.setTypeface(typeface2);
        tvTelStatic.setTypeface(typeface2);
        tvFullName.setTypeface(typeface2);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
            case R.id.fab:
                if (fabButton) {
                    editText();
                    fabButton = false;
                    edtLayout.setVisibility(View.VISIBLE);
                    showLayout.setVisibility(View.GONE);
                    fab.setImageResource(R.drawable.ic_save_24dp);
                    Snackbar.make(v, "Edit your profile", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                } else {
                    saveText();
                    showLayout.setVisibility(View.VISIBLE);
                    edtLayout.setVisibility(View.GONE);
                    fab.setImageResource(R.drawable.ic_edit_24dp);
                    fabButton = true;
                }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    protected void editText() {
//        editTextName.setText(textViewName.getText().toString());
//        editTextLastName.setText(textViewLastName.getText().toString());
//        editTextEmail.setText(textViewEmail.getText().toString());
    }

    protected void saveText() {
//        textViewName.setText(editTextName.getText().toString());
//        textViewLastName.setText(editTextLastName.getText().toString());
//        textViewEmail.setText(editTextEmail.getText().toString());

    }

    @Override
    public void processUser(String output) {
        Log.d("API user", output);

        try {
            JSONObject mainObject = new JSONObject(output);
            String name = mainObject.getString("name");
            String last_name = mainObject.getString("lastname");
            String email = mainObject.getString("email");
            String avatar = mainObject.getString("avatar");
            String fullname = name + " " + last_name;

            tvFullName.setText(fullname);
            tvName.setText(name);
            tvLastName.setText(last_name);
            tvEmail.setText(email);
            Glide.with(this).load(host + avatar).into(ivAvatar);

        } catch (Exception e) {

        }
    }

}
