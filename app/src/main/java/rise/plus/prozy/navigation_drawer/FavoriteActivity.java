package rise.plus.prozy.navigation_drawer;

import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;
import rise.plus.prozy.navigation_drawer.list.IndexAdapter;
import rise.plus.prozy.navigation_drawer.list.RecyclerItemClickListener;
import rise.plus.prozy.navigation_drawer.list.index;

import static rise.plus.prozy.Constant.UID;

public class FavoriteActivity extends ProzyActivity {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<index> dataset = new ArrayList<index>();

    //    ArrayList<String> item = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        recyclerView();
        navigationView2();

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new IndexAdapter(this, initIndex());
        mRecyclerView.setAdapter(mAdapter);
    }

    private List<index> initIndex() {

        for (int i = 1; i <= 5; i++) {
            index item = new index("", i , i , i);
            dataset.add(item);
        }

        return dataset;
    }

    private void recyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_favorite);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this,
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void MainPage() {
    }

    public void navigationView2() {
        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        Menu navMenu = navigationView.getMenu();

        if (UID != null) {
            navMenu.findItem(R.id.nav_profile).setVisible(true);
        } else {
            navMenu.findItem(R.id.nav_profile).setVisible(false);
        }

        if (UID != null) {
            navMenu.findItem(R.id.nav_login).setVisible(false);
            navMenu.findItem(R.id.nav_logout).setVisible(true);
            navMenu.findItem(R.id.nav_favorite).setVisible(true);
        } else {
            navMenu.findItem(R.id.nav_login).setVisible(true);
            navMenu.findItem(R.id.nav_logout).setVisible(false);
            navMenu.findItem(R.id.nav_favorite).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(this);

    }
}
