package rise.plus.prozy.navigation_drawer.list;

public class index {
    private String name;
    private float price;
    private float distance;
    private int rating;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getDistance() {
        return price;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }


    public index(String name, float price, float distance, int rating) {
        this.name = name;
        this.price = price;
        this.distance = distance;
        this.rating = rating;
    }


    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
