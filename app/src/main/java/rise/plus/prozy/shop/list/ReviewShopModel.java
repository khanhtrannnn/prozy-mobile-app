package rise.plus.prozy.shop.list;


public class ReviewShopModel {
    String title;
    String message;
    int rating;
    String date;
    String user;
    int reviewShopId;
    int userIdReviewShop;
    int userIdShop;

    public ReviewShopModel(String title, String message, int rating, String date, String user, int reviewShopId, int userIdReviewShop, int userIdShop) {
        this.title = title;
        this.message = message;
        this.rating = rating;
        this.date = date;
        this.user = user;
        this.reviewShopId = reviewShopId;
        this.userIdReviewShop = userIdReviewShop;
        this.userIdShop = userIdShop;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getReviewShopId() {
        return reviewShopId;
    }

    public void setReviewShopId(int reviewShopId) {
        this.reviewShopId = reviewShopId;
    }

    public int getUserIdReviewShop() {
        return userIdReviewShop;
    }

    public void setUserIdReviewShop(int userIdReviewShop) {
        this.userIdReviewShop = userIdReviewShop;
    }

    public int getUserIdShop() {
        return userIdShop;
    }

    public void setUserIdShop(int userIdShop) {
        this.userIdShop = userIdShop;
    }
}
