package rise.plus.prozy.shop;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rise.plus.prozy.product.fragment.ReviewProductFragment;
import rise.plus.prozy.shop.fragment.DetailShopFragment;
import rise.plus.prozy.shop.fragment.ProductOfShopFragment;
import rise.plus.prozy.shop.fragment.ReviewShopFragment;

public class TabAdapter extends FragmentStatePagerAdapter {

    private int TOTAL_TABS = 3;

    public TabAdapter(FragmentManager fm) {
        super(fm);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Fragment getItem(int index) {
        // TODO Auto-generated method stub
        switch (index) {
            case 0:
                return new DetailShopFragment();
            case 1:
                return new ProductOfShopFragment();
            case 2:
                return new ReviewShopFragment();

        }

        return null;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return TOTAL_TABS;
    }

}