package rise.plus.prozy.shop.list;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import rise.plus.prozy.R;

import static rise.plus.prozy.Constant.host;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private List<ProductModel> mIndices;
    private static Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPrice;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.search_name);
            tvPrice = (TextView) v.findViewById(R.id.search_price);
            ivImage = (ImageView) v.findViewById(R.id.search_image);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
            tvName.setTypeface(typeface);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductAdapter(Context context, List<ProductModel> dataset) {
        mIndices = dataset;
        mContext = context;

    }


    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.recycler_list_search, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ProductModel productModel = mIndices.get(position);

        holder.tvName.setText(productModel.getName());
        holder.tvPrice.setText("$" + productModel.getPrice().toString());


        if (productModel.getPrice() == -1.0) {
            holder.tvPrice.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.GONE);
        } else {

//            Log.d("shop", String.valueOf(productModel.getImgUrl()));
//            holder.tvPrice.setText("");

//            String imgUrl = host + productModel.getImgUrl() +"";
            Glide.with(mContext).load(host + productModel.getImgUrl()).into(holder.ivImage);
        }

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}
