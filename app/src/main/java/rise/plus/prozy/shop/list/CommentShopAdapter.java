package rise.plus.prozy.shop.list;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import rise.plus.prozy.R;


public class CommentShopAdapter extends RecyclerView.Adapter<CommentShopAdapter.ViewHolder> {

    private List<CommentShopModel> mIndices;
    private static Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public RatingBar rating;
        public TextView tvDate;
        public TextView tvMessage;
        public TextView tvUser;

        public ViewHolder(View view) {
            super(view);

            tvMessage = (TextView) view.findViewById(R.id.comment_message);
            rating = (RatingBar) view.findViewById(R.id.comment_rating);
            tvDate = (TextView) view.findViewById(R.id.comment_date);
            tvUser = (TextView) view.findViewById(R.id.comment_user);

            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Semibold.otf");
            tvUser.setTypeface(typeface);
//            Typeface typeface2 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
//            tvMessage.setTypeface(typeface2);
        }
    }

    public CommentShopAdapter(Context context, List<CommentShopModel> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_shop_comment, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        CommentShopModel index = mIndices.get(position);

        viewHolder.tvDate.setText(index.getDate());
        viewHolder.tvMessage.setText(index.getMessage());
        viewHolder.rating.setRating(index.getRating());
        viewHolder.tvUser.setText(index.getUser());

        if(index.getUserIdShop() == index.getUserIdCommentShop()){
            viewHolder.rating.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}