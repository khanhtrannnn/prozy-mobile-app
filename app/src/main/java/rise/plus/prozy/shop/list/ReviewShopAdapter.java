package rise.plus.prozy.shop.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.List;

import rise.plus.prozy.R;


public class ReviewShopAdapter extends RecyclerView.Adapter<ReviewShopAdapter.ViewHolder> {

    private List<ReviewShopModel> mIndices;
    private Context mContext;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDate, tvMessage, tvUser;
        public RatingBar rating;

        public ViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.review_title);
            tvMessage = (TextView) view.findViewById(R.id.review_message);
            rating = (RatingBar) view.findViewById(R.id.review_rating);
            tvDate = (TextView) view.findViewById(R.id.review_date);
            tvUser = (TextView) view.findViewById(R.id.review_user);
        }
    }

    public ReviewShopAdapter(Context context, List<ReviewShopModel> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_shop_review, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ReviewShopModel index = mIndices.get(position);

        viewHolder.tvDate.setText(index.getDate());
//        viewHolder.tvMessage.setText(index.getMessage());
        viewHolder.tvMessage.setText(Html.fromHtml(index.getMessage()));
        viewHolder.rating.setRating(index.getRating());
        viewHolder.tvUser.setText(index.getUser());
        viewHolder.tvTitle.setText(index.getTitle());

        if (index.getUserIdReviewShop() == index.getUserIdShop()) {
            viewHolder.rating.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}