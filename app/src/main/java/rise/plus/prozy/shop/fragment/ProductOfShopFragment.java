package rise.plus.prozy.shop.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.product.ProductActivity;
import rise.plus.prozy.product.list.RecyclerItemClickListener;
import rise.plus.prozy.services.ProductGet;
import rise.plus.prozy.services.async_response.ProductResponse;
import rise.plus.prozy.shop.list.ProductAdapter;
import rise.plus.prozy.shop.list.ProductModel;


public class ProductOfShopFragment extends Fragment implements View.OnClickListener, ProductResponse {
    private View view;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;
    private ProductModel item;
    List<ProductModel> dataset = new ArrayList<ProductModel>();
    private TextView mCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_product_of_shop, container, false);
        init(view);
        recyclerView();

        Bundle bundle = getActivity().getIntent().getExtras();
        int shopId = bundle.getInt("shop-id-item-clicked");

        new ProductGet(this, "/products.json?shop_id=" + shopId).execute();

        return view;

    }


    private void init(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_product_shop);
        mCount = (TextView) view.findViewById(R.id.count);
    }


    @Override
    public void onClick(View v) {

    }


    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                int ProductIdItemClicked = dataset.get(position).getProductId();

                Log.d("ProductModel-id", String.valueOf(ProductIdItemClicked) + " at position "
                        + String.valueOf(position));

                Intent intent = new Intent(getActivity().getApplication(), ProductActivity.class);
                intent.putExtra("product-id-item-clicked", ProductIdItemClicked);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));
    }

    private List<ProductModel> initIndex() {
        return dataset;
    }

    @Override
    public void processProduct(String output) {
        Log.d("API product", output);

        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                String name = jsonObj.getString("name");
                Double price = jsonObj.getDouble("price");
                String img = jsonObj.getString("image");
                int productId = jsonObj.getInt("id");
                int shopId = jsonObj.getJSONObject("shop").getInt("id");

                int countProduct = jsonArray.length();
                if (countProduct == 1) {
                    mCount.setText(String.valueOf(countProduct) + " product");
                } else if (countProduct > 1) {
                    mCount.setText(String.valueOf(countProduct) + " products");
                }

                item = new ProductModel(productId, shopId, name, price, 4, img);

                dataset.add(item);

            }

            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            mAdapter = new ProductAdapter(getActivity(), initIndex());
            mRecyclerView.setAdapter(mAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }

    }

}
