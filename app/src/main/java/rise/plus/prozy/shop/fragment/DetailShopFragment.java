package rise.plus.prozy.shop.fragment;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.product.list.RecyclerItemClickListener;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.async_response.CommentShopResponse;
import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.services.async_response.ShopResponse;
import rise.plus.prozy.services.CommentShopGet;
import rise.plus.prozy.services.ShopGet;
import rise.plus.prozy.shop.list.CommentShopAdapter;
import rise.plus.prozy.shop.list.CommentShopModel;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.userIdShop;
import static rise.plus.prozy.Constant.userIdLogged;


public class DetailShopFragment extends Fragment implements View.OnClickListener, ShopResponse,
        CommentShopResponse, PostResponse {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private TextView tvShopName, tvShopCategory, tvShopEmail, tvShopPhone, tvShopDescription;
    private RatingBar ratingBar;
    List<CommentShopModel> dataset = new ArrayList<CommentShopModel>();
    private float averageRatingShop;
    private View view;
    int shopId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_detail_shop, container, false);
        init(view);
        recyclerView();
        textStyle();

        Bundle bundle = getActivity().getIntent().getExtras();
        shopId = bundle.getInt("shop-id-item-clicked");

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }

        new ShopGet(this, "/shops/" + shopId + ".json").execute();
        new CommentShopGet(this, "/comment_shops/?shop_id=" + shopId + ".json").execute();

        return view;
    }

    private void init(View view) {

        view.findViewById(R.id.btn_comment).setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_comment_shop);
        tvShopName = (TextView) view.findViewById(R.id.shop_name);
        tvShopCategory = (TextView) view.findViewById(R.id.shop_category);
        tvShopEmail = (TextView) view.findViewById(R.id.shop_email);
        tvShopPhone = (TextView) view.findViewById(R.id.shop_phone);
        tvShopDescription = (TextView) view.findViewById(R.id.shop_description);
        ratingBar = (RatingBar) view.findViewById(R.id.shop_rating_bar);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_comment:
                comment();
                break;
        }
    }

    private void textStyle() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Diodrum Medium.otf");

        Typeface typeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Diodrum Semibold.otf");
        tvShopName.setTypeface(typeface2);
    }

    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                // ...
                Toast.makeText(getActivity(), "go to user " + (position + 1), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));

    }

    private void comment() {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_comment_post, null);
        builder.setView(view);

        final EditText edtMessage = (EditText) view.findViewById(R.id.message);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

        if (userIdLogged == userIdShop) {
            ratingBar.setVisibility(View.GONE);
        }

        builder.setPositiveButton("Comment", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String message = edtMessage.getText().toString();
                float rating = ratingBar.getRating();
                Date date = new Date();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("message", message);
                    jsonObject.put("rating", rating);
                    jsonObject.put("date", date);
                    jsonObject.put("user_id", UID);
                    jsonObject.put("shop_id", shopId);

                    Log.d("Comment", String.valueOf(jsonObject));

                    new ServicePost(DetailShopFragment.this, "/comment_shops.json",
                            String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    @Override
    public void processShop(String output) {
        Log.d("API shop", output);

        try {
            JSONObject mainObject = new JSONObject(output);
            String name = mainObject.getString("name");
            String category = mainObject.getJSONObject("category_shop").getString("name");
            String email = mainObject.getString("email");
            String phone = mainObject.getString("phone");
            String description = mainObject.getString("description");
            userIdShop = mainObject.getJSONObject("user").getInt("id");

            tvShopName.setText(name);
            tvShopCategory.setText(category);
            tvShopEmail.setText(email);
            tvShopPhone.setText(phone);
            tvShopDescription.setText(description);

        } catch (Exception e) {

        }
    }

    private List<CommentShopModel> initIndex() {
        return dataset;
    }

    @Override
    public void processCommentShop(String output) {
        Log.d("API comment_shops", output);
        dataset.clear();
        int count = 0;

        try {
            JSONArray jsonArray = new JSONArray(output);

            float sumRating = 0;

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String username = mainObject.getJSONObject("user").getString("name");
                String message = mainObject.getString("message");
                String date = mainObject.getString("date");
                int rating = mainObject.getInt("rating");
                int userIdCommentShop = mainObject.getJSONObject("user").getInt("id");

                if (userIdShop != userIdCommentShop) {
                    sumRating += rating;
                    count++;
                }

                Log.d("COMMENT SHOP", "user_id_shop: " + String.valueOf(userIdShop) + ", user_id_comment: "
                        + userIdCommentShop + ", user_id_logged: " + userIdLogged);

                CommentShopModel item = new CommentShopModel(message, rating, date, username, userIdCommentShop, userIdShop);
                dataset.add(item);
            }

            averageRatingShop = sumRating / count;
            ratingBar.setRating(averageRatingShop);
            Log.d("Average of rating shop", String.valueOf(averageRatingShop));

            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CommentShopAdapter(getActivity(), initIndex());
            mRecyclerView.setAdapter(mAdapter);

        } catch (Exception e) {

        }
    }

    @Override
    public void processPost(String output) {
        new CommentShopGet(this, "/comment_shops?shop_id=" + shopId + ".json").execute();
    }
}
