package rise.plus.prozy.shop;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import rise.plus.prozy.BaseActivity;
import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;
import rise.plus.prozy.shop.fragment.DetailShopFragment;
import rise.plus.prozy.shop.fragment.ProductOfShopFragment;
import rise.plus.prozy.shop.fragment.ReviewShopFragment;

public class ShopActivity extends BaseActivity implements View.OnClickListener{

    private String tabTitles[] = new String[]{"DETAIL", "PRODUCT", "REVIEW"};
    private ViewPager pager;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        navigationView();
        showToolBar(toolbar, this);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);

        PagerAdapter adapter = new HomeFragmentPagerAdapter(getSupportFragmentManager());
        pager = (ViewPager) findViewById(R.id.pager);

        tabLayout = (TabLayout) findViewById(R.id.tab);
        tabLayout.setupWithViewPager(pager);

        pager.setOffscreenPageLimit(3);
        pager.setAdapter(adapter);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(ShopActivity.this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showToolBar(Toolbar toolbar, final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        @SuppressWarnings("deprecation")
        Drawable drawable = activity.getResources().getDrawable(
                R.drawable.prozy_logo);
        drawable.setColorFilter(
                activity.getResources().getColor(R.color.colorWhite),
                android.graphics.PorterDuff.Mode.SRC_ATOP);

        ImageView prozyLogo = (ImageView) findViewById(R.id.toolbar_logo);
        prozyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProzyActivity.class);
                startActivity(intent);
            }
        });
    }

    public class HomeFragmentPagerAdapter extends FragmentPagerAdapter {
        private final int PAGE_COUNT = 3;

        public HomeFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int page) {
            switch (page) {
                case 0:
                    return new DetailShopFragment();
                case 1:
                    return new ProductOfShopFragment();
                case 2:
                    return new ReviewShopFragment();
            }
            return null;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            SpannableString sb = new SpannableString(" " + tabTitles[position]);
            return sb;
        }
    }

}
