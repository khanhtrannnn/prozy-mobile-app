package rise.plus.prozy.shop.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.product.list.RecyclerItemClickListener;
import rise.plus.prozy.review.DetailReviewShopActivity;
import rise.plus.prozy.services.ReviewShopGet;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.services.async_response.ReviewShopResponse;
import rise.plus.prozy.shop.list.ReviewShopAdapter;
import rise.plus.prozy.shop.list.ReviewShopModel;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.userIdShop;
import static rise.plus.prozy.Constant.userIdLogged;

public class ReviewShopFragment extends Fragment implements View.OnClickListener,
        ReviewShopResponse, PostResponse {

    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<ReviewShopModel> dataset = new ArrayList<ReviewShopModel>();
    private int userId = 0, shopId;
    private int reviewShopIdItemClicked;
    private TextView mCount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_review_shop, container, false);
        init(view);
        recyclerView();

        Bundle bundle = getActivity().getIntent().getExtras();
        shopId = bundle.getInt("shop-id-item-clicked");

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }
        new ReviewShopGet(this, "/review_shops.json?shop_id=" + shopId).execute();

        return view;

    }

    private void init(View view) {
        view.findViewById(R.id.btn_review).setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_review_shop);
        mCount = (TextView) view.findViewById(R.id.count);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_review:
                review();
                break;
        }
    }

    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                reviewShopIdItemClicked = dataset.get(position).getReviewShopId();
                Intent intent = new Intent(view.getContext(), DetailReviewShopActivity.class);
                intent.putExtra("review-shop-id-item-clicked", reviewShopIdItemClicked);
                Log.d("Review-shop-id", String.valueOf(reviewShopIdItemClicked));
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                // ...
            }
        }));
    }

    private void review() {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_review_post, null);
        builder.setView(view);

        final EditText edtMessage = (EditText) view.findViewById(R.id.message);
        final EditText edtTitle = (EditText) view.findViewById(R.id.title);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

        if (userIdLogged == userIdShop) {
            ratingBar.setVisibility(View.GONE);
        }

        builder.setPositiveButton("Review", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = edtTitle.getText().toString();
                String message = edtMessage.getText().toString();
                float rating = ratingBar.getRating();
                Date date = new Date();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("title", title);
                    jsonObject.put("message", message);
                    jsonObject.put("rating", rating);
                    jsonObject.put("date", date);
                    jsonObject.put("user_id", UID);
                    jsonObject.put("shop_id", shopId);

                    Log.d("Review", String.valueOf(jsonObject));

                    new ServicePost(ReviewShopFragment.this, "/review_shops.json",
                            String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }


    private List<ReviewShopModel> initIndex() {
        return dataset;
    }

    @Override
    public void processReviewShop(String output) {
        Log.d("API review_shops", output);
        dataset.clear();
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String username = mainObject.getJSONObject("user").getString("name");
                String title = mainObject.getString("title");
                String message = mainObject.getString("message");
                String date = mainObject.getString("date");
                int rating = mainObject.getInt("rating");
                int userIdReviewShop = mainObject.getJSONObject("user").getInt("id");
                int reviewShopId = mainObject.getInt("id");

                int countReview = jsonArray.length();
                if (countReview == 1) {
                    mCount.setText(String.valueOf(countReview) + " review");
                } else if (countReview > 1) {
                    mCount.setText(String.valueOf(countReview) + " reviews");
                }

                Log.d("REVIEW SHOP", "user_id_shop: " + String.valueOf(userIdShop) + ", user_id_review: "
                        + userIdReviewShop + ", user_id_logged: " + userIdLogged);
                ReviewShopModel item =
                        new ReviewShopModel(title, message, rating, date, username, reviewShopId, userIdReviewShop, userIdShop);
                dataset.add(item);
            }

            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new ReviewShopAdapter(getActivity(), initIndex());
            mRecyclerView.setAdapter(mAdapter);

        } catch (Exception e) {

        }
    }

    @Override
    public void processPost(String output) {
        new ReviewShopGet(this, "/review_shops?shop_id=" + shopId + ".json").execute();
    }
}

