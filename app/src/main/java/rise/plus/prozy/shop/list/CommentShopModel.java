package rise.plus.prozy.shop.list;

public class CommentShopModel {

    String message;
    int rating;
    String date;
    String user;
    int userIdCommentShop;
    int userIdShop;

    public CommentShopModel(String message, int rating, String date, String user, int userIdCommentShop, int userIdShop) {
        this.message = message;
        this.rating = rating;
        this.date = date;
        this.user = user;
        this.userIdCommentShop = userIdCommentShop;
        this.userIdShop = userIdShop;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getUserIdCommentShop() {
        return userIdCommentShop;
    }

    public void setUserIdCommentShop(int userIdCommentShop) {
        this.userIdCommentShop = userIdCommentShop;
    }

    public int getUserIdShop() {
        return userIdShop;
    }

    public void setUserIdShop(int userIdShop) {
        this.userIdShop = userIdShop;
    }
}