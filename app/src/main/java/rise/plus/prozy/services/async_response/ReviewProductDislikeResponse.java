package rise.plus.prozy.services.async_response;

public interface ReviewProductDislikeResponse {

    void processReviewProductDislike(String output);

}
