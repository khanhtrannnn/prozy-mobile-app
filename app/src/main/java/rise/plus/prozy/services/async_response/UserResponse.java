package rise.plus.prozy.services.async_response;

public interface UserResponse {

    void processUser(String output);

}
