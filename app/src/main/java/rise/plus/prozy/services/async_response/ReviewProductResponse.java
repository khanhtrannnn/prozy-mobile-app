package rise.plus.prozy.services.async_response;

public interface ReviewProductResponse {

    void processReviewProduct(String output);

}
