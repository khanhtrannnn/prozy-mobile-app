package rise.plus.prozy.services;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import rise.plus.prozy.services.async_response.ReviewShopDislikeResponse;

import static rise.plus.prozy.Constant.host;

public class ReviewShopDislikeGet extends AsyncTask<Void, Void, String> {
    private String url;
    public ReviewShopDislikeResponse delegate = null;

    public ReviewShopDislikeGet(ReviewShopDislikeResponse asyncResponse, String url) {
        delegate = asyncResponse;
        this.url = url;
    }

    @Override
    protected String doInBackground(Void... voids) {
        OkHttpClient okHttpClient = new OkHttpClient();

        Request.Builder builder = new Request.Builder();
        Request request = builder.url(host + url).build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.d("status", "Get API-review-product-dislike successful");
                return response.body().string();
            } else {
                Log.d("status", "fail");
                return "Get API-review-shop-dislike unsuccessful - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error - " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);

        delegate.processReviewShopDislike(string);

    }


}


