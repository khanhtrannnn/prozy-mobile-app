package rise.plus.prozy.services.async_response;

public interface UpdateResponse {
    void processUpdate(String output);
}
