package rise.plus.prozy.services;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import rise.plus.prozy.services.async_response.ReviewShopLikeResponse;

import static rise.plus.prozy.Constant.host;

public class ReviewShopLikeGet extends AsyncTask<Void, Void, String> {
    private String url;
    public ReviewShopLikeResponse delegate = null;

    public ReviewShopLikeGet(ReviewShopLikeResponse asyncResponse, String url) {
        delegate = asyncResponse;
        this.url = url;
    }

    @Override
    protected String doInBackground(Void... voids) {
        OkHttpClient okHttpClient = new OkHttpClient();

        Request.Builder builder = new Request.Builder();
        Request request = builder.url(host + url).build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.d("status", "Get API-review-product-like successful");
                return response.body().string();
            } else {
                Log.d("status", "fail");
                return "Get API-review-shop-like unsuccessful - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error - " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);

        delegate.processReviewShopLike(string);

    }


}


