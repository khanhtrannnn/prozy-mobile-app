package rise.plus.prozy.services.async_response;

public interface ReviewProductLikeResponse {

    void processReviewProductLike(String output);

}
