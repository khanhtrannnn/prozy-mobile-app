package rise.plus.prozy.services.async_response;

public interface PromotionResponse {

    void processPromotion(String output);

}
