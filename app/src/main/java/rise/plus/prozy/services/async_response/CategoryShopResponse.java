package rise.plus.prozy.services.async_response;

public interface CategoryShopResponse {

    void processCategoryShop(String output);

}
