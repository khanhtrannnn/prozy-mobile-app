package rise.plus.prozy.services.async_response;

public interface ReviewShopLikeResponse {

    void processReviewShopLike(String output);

}
