package rise.plus.prozy.services.async_response;

public interface CommentProductResponse {

    void processCommentProduct(String output);
}
