package rise.plus.prozy.services.async_response;

public interface CommentShopResponse {

    void processCommentShop(String output);

}
