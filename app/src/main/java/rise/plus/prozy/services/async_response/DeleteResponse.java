package rise.plus.prozy.services.async_response;

public interface DeleteResponse {
    void processDelete();
}
