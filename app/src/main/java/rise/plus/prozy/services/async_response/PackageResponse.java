package rise.plus.prozy.services.async_response;

public interface PackageResponse {

    void processPackage(String output);

}
