package rise.plus.prozy.services;

import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import rise.plus.prozy.services.async_response.DeleteResponse;

import static rise.plus.prozy.Constant.host;

public class ServiceDelete extends AsyncTask<Void, Void, String> {

    private String url;
    public DeleteResponse delegate = null;

    public ServiceDelete(DeleteResponse asyncResponse, String url) {
        this.delegate = asyncResponse;
        this.url = url;
    }

    @Override
    protected String doInBackground(Void... params) {
        OkHttpClient okHttpClient = new OkHttpClient();

        Request request = new Request.Builder()
                .url(host + url)
                .delete()
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.d("Service delete", "successful");
                return response.body().string();
            } else {
                Log.d("Service delete", "fail");
                return "Not Success - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error - " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);

        delegate.processDelete();

//        Log.d("String", string);

    }
}
