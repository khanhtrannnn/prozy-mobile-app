package rise.plus.prozy.services.async_response;

public interface ProductResponse {

    void processProduct(String output);

}
