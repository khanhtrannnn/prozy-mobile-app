package rise.plus.prozy.services.async_response;

public interface ShopResponse {

    void processShop(String output);

}
