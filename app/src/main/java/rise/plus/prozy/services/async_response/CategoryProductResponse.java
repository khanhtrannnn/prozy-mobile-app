package rise.plus.prozy.services.async_response;

public interface CategoryProductResponse {

    void processCategoryProduct(String output);

}
