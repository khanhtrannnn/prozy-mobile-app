package rise.plus.prozy.services.async_response;

public interface PostResponse {

    void processPost(String output);

}
