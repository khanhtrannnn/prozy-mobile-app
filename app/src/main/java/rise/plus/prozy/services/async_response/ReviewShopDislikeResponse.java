package rise.plus.prozy.services.async_response;

public interface ReviewShopDislikeResponse {

    void processReviewShopDislike(String output);

}
