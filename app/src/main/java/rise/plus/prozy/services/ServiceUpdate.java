package rise.plus.prozy.services;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;

import rise.plus.prozy.services.async_response.UpdateResponse;

import static rise.plus.prozy.Constant.host;

public class ServiceUpdate extends AsyncTask<Void, Void, String> {

    private String url;
    private String json;
    public UpdateResponse delegate = null;

    public static final MediaType JSON  = MediaType.parse("application/json; charset=utf-8");

    public ServiceUpdate(UpdateResponse asyncResponse, String url , String json) {
        this.delegate = asyncResponse;
        this.url = url;
        this.json = json;
    }

    public ServiceUpdate(DialogInterface.OnClickListener onClickListener, String url, String json) {
        this.url = url;
        this.json = json;
    }

    @Override
    protected String doInBackground(Void... params) {
        OkHttpClient okHttpClient = new OkHttpClient();

        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(host + url)
                .patch(body)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                Log.d("Service update", "successful");
                return response.body().string();
            } else {
                Log.d("Service update", "fail");
                return "Not Success - code : " + response.code();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "Error - " + e.getMessage();
        }
    }

    @Override
    protected void onPostExecute(String string) {
        super.onPostExecute(string);

        delegate.processUpdate(string);

//        Log.d("String", string);

    }
}
