package rise.plus.prozy.services.async_response;

public interface ReviewShopResponse {

    void processReviewShop(String output);

}
