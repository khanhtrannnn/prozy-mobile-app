package rise.plus.prozy.package_promotion.list;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import rise.plus.prozy.R;

import static rise.plus.prozy.Constant.host;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolder> {

    private List<PromotionModel> mIndices;
    private static Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvPrice, tvDiscount, tvNewPrice;
        public ImageView ivImage;

        public ViewHolder(View view) {
            super(view);

            ivImage = (ImageView) view.findViewById(R.id.promotion_image);
            tvName = (TextView) view.findViewById(R.id.promotion_name);
            tvPrice = (TextView) view.findViewById(R.id.promotion_price);
            tvDiscount = (TextView) view.findViewById(R.id.promotion_discount);
            tvNewPrice = (TextView) view.findViewById(R.id.promotion_new_price);
            tvPrice.setPaintFlags(tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
            tvName.setTypeface(typeface);
        }
    }

    public PromotionAdapter(Context context, List<PromotionModel> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_promotion, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        PromotionModel index = mIndices.get(position);

        Glide.with(mContext).load(host + index.getImage()).into(viewHolder.ivImage);
        viewHolder.tvName.setText(index.getName());
        viewHolder.tvPrice.setText("$" + index.getPrice().toString());
        viewHolder.tvDiscount.setText("- " + index.getDiscount().toString() + "%");
        viewHolder.tvNewPrice.setText("$" + String.format("%.2f", index.getNewPrice()));

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}
