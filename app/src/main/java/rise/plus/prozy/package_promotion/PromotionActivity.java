package rise.plus.prozy.package_promotion;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.BaseActivity;
import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;
import rise.plus.prozy.package_promotion.list.PromotionAdapter;
import rise.plus.prozy.package_promotion.list.PromotionModel;
import rise.plus.prozy.product.ProductActivity;
import rise.plus.prozy.services.PackageGet;
import rise.plus.prozy.services.PromotionGet;
import rise.plus.prozy.services.async_response.PackageResponse;
import rise.plus.prozy.services.async_response.PromotionResponse;

import static rise.plus.prozy.Constant.host;

public class PromotionActivity extends BaseActivity implements PromotionResponse, PackageResponse, View.OnClickListener {

    List<PromotionModel> dataset = new ArrayList<PromotionModel>();
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private int packageId;
    private float totalPrice, totalNewPrice;
    private TextView tvTotalPrice, tvTotalNewPrice, tvTotal, mCount, tvPackageName;
    private ImageView promotionPicture;
    private int promotionItemClicked;
    private ImageView ivBack;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);

        Bundle bundle = getIntent().getExtras();
        packageId = bundle.getInt("package-id");

        init();
        recyclerView();
        showToolBar(toolbar, this);
        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_image));
        navigationView();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(PromotionActivity.this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();

        new PackageGet(this, "/packages/" + packageId + ".json").execute();
        new PromotionGet(this, "/promotions.json?package_id=" + packageId).execute();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/JosefinSans Bold.ttf");
        tvTotal.setTypeface(typeface);
    }

    private void init() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_promotion);
        tvTotalPrice = (TextView) findViewById(R.id.total_price);
        tvTotalNewPrice = (TextView) findViewById(R.id.total_new_price);
        tvTotalPrice.setPaintFlags(tvTotalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        promotionPicture = (ImageView) findViewById(R.id.promotion_image);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTotal = (TextView) findViewById(R.id.tv_total);
        mCount = (TextView) findViewById(R.id.count);
        tvPackageName = (TextView) findViewById(R.id.package_name);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showToolBar(Toolbar toolbar, final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        @SuppressWarnings("deprecation")
        Drawable drawable = activity.getResources().getDrawable(
                R.drawable.prozy_logo);
        drawable.setColorFilter(
                activity.getResources().getColor(R.color.colorWhite),
                android.graphics.PorterDuff.Mode.SRC_ATOP);

        ImageView prozyLogo = (ImageView) findViewById(R.id.toolbar_logo);
        prozyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProzyActivity.class);
                startActivity(intent);
            }
        });
    }

    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new rise.plus.prozy.product.list.RecyclerItemClickListener
                (this, mRecyclerView, new rise.plus.prozy.product.list.RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        promotionItemClicked = dataset.get(position).getProductId();
                        Intent intent = new Intent(view.getContext(), ProductActivity.class);
                        intent.putExtra("product-id-item-clicked", promotionItemClicked);
//                        Log.d("Promotion_itemclicked", String.valueOf(promotionItemClicked));
                        startActivity(intent);

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));
    }

    private List<PromotionModel> initIndex() {
        return dataset;
    }


    @Override
    public void processPackage(String output) {
        Log.d("API package", output);
        dataset.clear();

        try {
            JSONObject jsonObject = new JSONObject(output);
            String image = jsonObject.getString("image");
            String name = jsonObject.getString("name");

            tvPackageName.setText(name);
            Glide.with(this).load(host + image).into(promotionPicture);

        } catch (Exception e) {

        }
    }


    @Override
    public void processPromotion(String output) {
        Log.d("API promotion", output);
        dataset.clear();

        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getJSONObject("product").getString("image");
                String name = jsonObject.getJSONObject("product").getString("name");
                double price = jsonObject.getJSONObject("product").getDouble("price");
                int discount = jsonObject.getInt("discount");
                double newPrice = price * (100 - discount) / 100;
                int productId = jsonObject.getJSONObject("product").getInt("id");
                Log.d("API_promotion", image + name + price + discount);

                int countProduct = jsonArray.length();
                if (countProduct == 1) {
                    mCount.setText(String.valueOf(countProduct) + " product");
                } else if (countProduct > 1) {
                    mCount.setText(String.valueOf(countProduct) + " products");
                }

                totalPrice += price;
                totalNewPrice += newPrice;

                PromotionModel item =
                        new PromotionModel(image, name, price, discount, newPrice, productId);
                dataset.add(item);
            }

//            double i2=i/60000;
//            tv.setText(new DecimalFormat("##.##").format(i2));

            tvTotalPrice.setText("$" + new DecimalFormat("##.##").format(totalPrice));
            tvTotalNewPrice.setText("$" + new DecimalFormat("##.##").format(totalNewPrice));

            mLayoutManager = new LinearLayoutManager(PromotionActivity.this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new PromotionAdapter(PromotionActivity.this, initIndex());
            mRecyclerView.setAdapter(mAdapter);

        } catch (Exception e) {

        }
    }

}
