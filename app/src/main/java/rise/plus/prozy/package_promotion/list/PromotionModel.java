package rise.plus.prozy.package_promotion.list;
public class PromotionModel {
    private String image;
    private String name;
    private double price;
    private int discount;
    private double newPrice;
    private int productId;

    public PromotionModel(String image, String name, double price, int discount, double newPrice, int productId) {
        this.image = image;
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.newPrice = newPrice;
        this.productId = productId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public void setNewPrice(double newPrice) {
        this.newPrice = newPrice;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getImage() {
        return image;
    }

    public Double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Double getNewPrice() {
        return newPrice;
    }

    public int getProductId() {
        return productId;
    }


}
