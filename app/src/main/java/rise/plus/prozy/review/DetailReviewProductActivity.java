package rise.plus.prozy.review;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rise.plus.prozy.BaseActivity;
import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;
import rise.plus.prozy.services.ReviewProductDislikeGet;
import rise.plus.prozy.services.ReviewProductGet;
import rise.plus.prozy.services.ReviewProductLikeGet;
import rise.plus.prozy.services.async_response.ReviewProductDislikeResponse;
import rise.plus.prozy.services.async_response.ReviewProductLikeResponse;
import rise.plus.prozy.services.async_response.ReviewProductResponse;

public class DetailReviewProductActivity extends BaseActivity implements
        ReviewProductResponse, ReviewProductLikeResponse, ReviewProductDislikeResponse, View.OnClickListener {
    private TextView tvName, tvTitle, tvMessage, tvDate, totalLikes, totalDislikes;
    private RatingBar reviewRating;
    private int reviewProductId;
    private Toolbar toolbar;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_review);

        init();
        navigationView();
        showToolBar(toolbar, this);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        reviewProductId = bundle.getInt("review-product-id-item-clicked");

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(DetailReviewProductActivity.this, mDrawerLayout,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();

        new ReviewProductGet(this, "/review_products/" + reviewProductId + ".json").execute();
        new ReviewProductLikeGet(this, "/product_review_likes.json?review_product_id=" + reviewProductId).execute();
        new ReviewProductDislikeGet(this, "/product_review_dislikes.json?review_product_id=" + reviewProductId).execute();

        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Semibold.otf");
        tvName.setTypeface(typeface);

    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvName = (TextView) findViewById(R.id.review_username);
        tvTitle = (TextView) findViewById(R.id.review_title);
        tvMessage = (TextView) findViewById(R.id.review_message);
        tvDate = (TextView) findViewById(R.id.review_date);
        reviewRating = (RatingBar) findViewById(R.id.review_rating);
        totalLikes = (TextView) findViewById(R.id.total_likes);
        totalDislikes = (TextView) findViewById(R.id.total_dislikes);
    }

    @Override
    public void processReviewProduct(String output) {
        Log.d("API review-product", output);
        try {
            JSONObject jsonObject = new JSONObject(output);
            String username = jsonObject.getJSONObject("user").getString("name");
            String title = jsonObject.getString("title");
            String message = jsonObject.getString("message");
            int rating = jsonObject.getInt("rating");
            String date = jsonObject.getString("date");

            tvName.setText(username);
            tvTitle.setText(title);
            tvMessage.setText(message);
            tvDate.setText(date);
            reviewRating.setRating(rating);

        } catch (Exception e) {

        }
    }

    @Override
    public void processReviewProductLike(String output) {
        Log.d("API_review_product_like", output);
        try {
            JSONArray jsonArray = new JSONArray(output);
            int countLikes = jsonArray.length();
            totalLikes.setText(String.valueOf(countLikes));
            Log.d("tongsolike", (String.valueOf(countLikes)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void processReviewProductDislike(String output) {
        Log.d("API review-product-like", output);
        try {
            JSONArray jsonArray = new JSONArray(output);
            int countDislikes = jsonArray.length();
            totalDislikes.setText(String.valueOf(countDislikes));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back:
                finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showToolBar(Toolbar toolbar, final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        @SuppressWarnings("deprecation")
        Drawable drawable = activity.getResources().getDrawable(
                R.drawable.prozy_logo);
        drawable.setColorFilter(
                activity.getResources().getColor(R.color.colorWhite),
                android.graphics.PorterDuff.Mode.SRC_ATOP);

        ImageView prozyLogo = (ImageView) findViewById(R.id.toolbar_logo);
        prozyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProzyActivity.class);
                startActivity(intent);
            }
        });
    }
}
