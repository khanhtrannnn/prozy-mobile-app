package rise.plus.prozy.main.packages;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import rise.plus.prozy.R;
import rise.plus.prozy.main.MainPage;

public class Adapter extends RecyclerView.Adapter<Adapter.Holder> {

    Context mContext;
    int[] images;

    public class Holder extends RecyclerView.ViewHolder {

        ImageView img;

        public Holder(View itemView) {
            super(itemView);

            img = (ImageView) itemView.findViewById(R.id.imageView);
        }
    }

    public Adapter(Context context, int[] images) {
        this.mContext = context;
        this.images = images;

    }

    public Adapter(MainPage viewPagerActivity, int[] images) {
        this.images = images;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_product_main_page, null);
        Holder holder = new Holder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.img.setImageResource(images[position]);
    }

    @Override
    public int getItemCount() {
        return images.length;
    }
}
