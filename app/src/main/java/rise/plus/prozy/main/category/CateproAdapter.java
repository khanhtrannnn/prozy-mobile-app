package rise.plus.prozy.main.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import rise.plus.prozy.R;

import static rise.plus.prozy.Constant.host;

public class CateproAdapter extends RecyclerView.Adapter<CateproAdapter.ViewHolder> {

    private List<CateproModel> mIndices;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder  {

        public ImageView ivImage;
        public TextView tvName;

        public ViewHolder(View view) {
            super(view);
            ivImage = (ImageView) view.findViewById(R.id.grid_image);
//            tvName = (TextView) view.findViewById(R.id.grid_name);
        }
    }

    public CateproAdapter(Context context, List<CateproModel> dataset) {
        this.mIndices = dataset;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_list, null);
        ViewHolder viewHolder = new ViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        CateproModel index = mIndices.get(position);

//        viewHolder.tvName.setText(index.getName());
        Glide.with(mContext).load(host + index.getImage()).into(viewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        return this.mIndices.size();
    }
}

