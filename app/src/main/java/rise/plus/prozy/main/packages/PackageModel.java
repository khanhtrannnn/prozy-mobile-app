package rise.plus.prozy.main.packages;

public class PackageModel {
    String image;
    int productId;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public PackageModel(String image, int productId) {
        this.image = image;
        this.productId = productId;
    }

}
