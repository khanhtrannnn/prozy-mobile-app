package rise.plus.prozy.main.packages;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import rise.plus.prozy.R;
import rise.plus.prozy.navigation_drawer.ProfileActivity;
import rise.plus.prozy.package_promotion.PromotionActivity;
import rise.plus.prozy.search.SearchActivity;

import static rise.plus.prozy.Constant.host;


public class ImagePagerAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<String> arrayList;
//    List<PackageModel> arrayList = new ArrayList<PackageModel>();

    public ImagePagerAdapter(Context context, ArrayList<String> arrayList) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrayList = arrayList;
    }

//    public ImagePagerAdapter(Context context, List<PackageModel> arrayList) {
//        this.context = context;
//        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        this.arrayList = arrayList;
//    }

    @Override
    public int getCount() {
        if (arrayList != null) {
            return arrayList.size();
        }
        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.package_viewpager, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.package_image);

        String image = arrayList.get(position);

//        int productID = arrayList.get(position).getProductId();

        Glide.with(context).load(host + String.valueOf(image)).into(imageView);

        container.addView(itemView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PromotionActivity.class);
                intent.putExtra("package-id", position + 1);
                context.startActivity(intent);

            }
        });


        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
