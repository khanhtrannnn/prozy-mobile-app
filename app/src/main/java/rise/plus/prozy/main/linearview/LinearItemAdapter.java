package rise.plus.prozy.main.linearview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import rise.plus.prozy.R;

import static rise.plus.prozy.Constant.host;

public class LinearItemAdapter extends RecyclerView.Adapter<LinearItemAdapter.ViewHolder> {

    private List<LinearItemModel> mIndices;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder  {

        public ImageView ivImage;
        public TextView tvName;
        public TextView tvPrice;

        public ViewHolder(View view) {
            super(view);
            ivImage = (ImageView) view.findViewById(R.id.grid_image);
            tvName = (TextView) view.findViewById(R.id.grid_name);
            tvPrice = (TextView) view.findViewById(R.id.grid_price);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
            tvName.setTypeface(typeface);
        }
    }

    public LinearItemAdapter(Context context, List<LinearItemModel> dataset) {
        this.mIndices = dataset;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.linear_view_list, null);
        ViewHolder viewHolder = new ViewHolder(layoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        LinearItemModel index = mIndices.get(position);

        viewHolder.tvName.setText(index.getName());
        viewHolder.tvPrice.setText("$" + index.getPrice().toString());
        Glide.with(mContext).load(host + index.getImage()).into(viewHolder.ivImage);

    }

    @Override
    public int getItemCount() {
        return this.mIndices.size();
    }
}

