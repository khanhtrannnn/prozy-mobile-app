package rise.plus.prozy.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import rise.plus.prozy.R;
import rise.plus.prozy.main.category.CateproAdapter;
import rise.plus.prozy.main.category.CateproModel;
import rise.plus.prozy.main.category2.CateproAdapter2;
import rise.plus.prozy.main.category2.CateproModel2;
import rise.plus.prozy.main.category3.CateproAdapter3;
import rise.plus.prozy.main.category3.CateproModel3;
import rise.plus.prozy.main.linearview.LinearItemModel;
import rise.plus.prozy.main.linearview.LinearItemAdapter;
import rise.plus.prozy.main.packages.ImagePagerAdapter;
import rise.plus.prozy.main.linearview.RecyclerItemClickListener;
import rise.plus.prozy.product.ProductActivity;
import rise.plus.prozy.services.CategoryProductGet;
import rise.plus.prozy.services.CategoryShopGet;
import rise.plus.prozy.services.PackageGet;
import rise.plus.prozy.services.ProductGet;
import rise.plus.prozy.services.ShopGet;
import rise.plus.prozy.services.async_response.CategoryProductResponse;
import rise.plus.prozy.services.async_response.CategoryShopResponse;
import rise.plus.prozy.services.async_response.PackageResponse;
import rise.plus.prozy.services.async_response.ProductResponse;
import rise.plus.prozy.services.async_response.ShopResponse;

public class MainPage extends Fragment
        implements PackageResponse, ProductResponse, CategoryProductResponse, ShopResponse, CategoryShopResponse {

    private ViewPager viewpager;
    private View view;
    private RecyclerView mRecyclerView, mRecyclerView2, mRecyclerView3, mRecyclerView4;
    private RecyclerView.Adapter mAdapter, mAdapter2, mAdapter3, mAdapter4;
    private RecyclerView.LayoutManager mLayoutManager, mLayoutManager2, mLayoutManager3, mLayoutManager4;
    private ArrayList packages = new ArrayList();
    List<LinearItemModel> dataset = new ArrayList<LinearItemModel>();
    List<CateproModel> cateset1 = new ArrayList<CateproModel>();
    List<CateproModel2> cateset2 = new ArrayList<CateproModel2>();
    List<CateproModel3> cateset3 = new ArrayList<CateproModel3>();

    private int gridItemClicked, cateProItemClicked, cateProItemClicked2, cateProItemClicked3;
    private TextView tvCategory, tvCategory2, tvCategory3, tvDeal;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main_page, container, false);
        init(view);
        recyclerView();
        recyclerView2();
        recyclerView3();
        recyclerView4();

        new PackageGet(this, "/packages.json").execute();
        new ProductGet(this, "/products.json").execute();
        new CategoryProductGet(this, "/products.json?keyword=&category_product_id=1").execute();
        new ShopGet(this, "/products.json?keyword=&category_product_id=2").execute();
        new CategoryShopGet(this, "/products.json?keyword=&category_product_id=3").execute();

        return view;
    }

    private void init(View view) {
        tvCategory = (TextView) view.findViewById(R.id.tv_category);
        tvCategory2 = (TextView) view.findViewById(R.id.tv_category2);
        tvCategory3 = (TextView) view.findViewById(R.id.tv_category3);
        tvDeal = (TextView) view.findViewById(R.id.deal);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.grid_recycler_view);
        mRecyclerView2 = (RecyclerView) view.findViewById(R.id.catepro_recycler_view);
        mRecyclerView3 = (RecyclerView) view.findViewById(R.id.catepro_recycler_view2);
        mRecyclerView4 = (RecyclerView) view.findViewById(R.id.catepro_recycler_view3);
        viewpager = (ViewPager) view.findViewById(R.id.pager);

    }


    @Override
    public void processPackage(String output) {
        Log.d("API package", output);
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getString("image");
                packages.add(image);
                Log.d("packages_image", image);
            }

            viewpager.setOffscreenPageLimit(packages.size());
            Log.d("packages_size", String.valueOf(packages.size()));
            PagerAdapter adapter = new ImagePagerAdapter(getActivity(), packages);
            viewpager.setAdapter(adapter);

            CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
            indicator.setViewPager(viewpager);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


//    --------------------------------------------------------------------------------------------------------------

    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                gridItemClicked = cateset1.get(position).getProductId();
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product-id-item-clicked", gridItemClicked);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void recyclerView2() {

        mRecyclerView2.setHasFixedSize(true);
        mRecyclerView2.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView2, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                cateProItemClicked = cateset1.get(position).getProductId();
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product-id-item-clicked", cateProItemClicked);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void recyclerView3() {

        mRecyclerView3.setHasFixedSize(true);
        mRecyclerView3.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView3, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                cateProItemClicked2 = cateset2.get(position).getProductId();
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product-id-item-clicked", cateProItemClicked2);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void recyclerView4() {

        mRecyclerView4.setHasFixedSize(true);
        mRecyclerView4.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView4, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                cateProItemClicked3 = cateset3.get(position).getProductId();
                Intent intent = new Intent(view.getContext(), ProductActivity.class);
                intent.putExtra("product-id-item-clicked", cateProItemClicked3);
                startActivity(intent);
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }


    private List<LinearItemModel> initIndex() {
        return dataset;
    }


    @Override
    public void processProduct(String output) {
        Log.d("API grid_product", output);
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getString("image");
                String name = jsonObject.getString("name");
                int productId = jsonObject.getInt("id");
                double price = jsonObject.getDouble("price");

                LinearItemModel item = new LinearItemModel(name, image, price, productId);
                dataset.add(item);
            }


            tvDeal.setText("Deals of the day");
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new LinearItemAdapter(getActivity(), initIndex());
            mRecyclerView.setAdapter(mAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<CateproModel> initCate1() {
        return cateset1;
    }


    @Override
    public void processCategoryProduct(String output) {
        Log.d("API categorized_product", output);
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getString("image");
                String name = jsonObject.getString("name");
                String category_product_name = jsonObject.getJSONObject("category_product").getString("name");
                int productId = jsonObject.getInt("id");

                tvCategory.setText(category_product_name);

                CateproModel item = new CateproModel(name, image, productId);
                cateset1.add(item);
            }

            mLayoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            mRecyclerView2.setLayoutManager(mLayoutManager2);
            mAdapter2 = new CateproAdapter(getActivity(), initCate1());
            mRecyclerView2.setAdapter(mAdapter2);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<CateproModel2> initCate2() {
        return cateset2;
    }

    @Override
    public void processShop(String output) {
        Log.d("API categorized_product", output);
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getString("image");
                String name = jsonObject.getString("name");
                String category_product_name = jsonObject.getJSONObject("category_product").getString("name");
                int productId = jsonObject.getInt("id");

                tvCategory2.setText(category_product_name);

                CateproModel2 item = new CateproModel2(name, image, productId);
                cateset2.add(item);
            }

            mLayoutManager3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            mRecyclerView3.setLayoutManager(mLayoutManager3);
            mAdapter3 = new CateproAdapter2(getActivity(), initCate2());
            mRecyclerView3.setAdapter(mAdapter3);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<CateproModel3> initCate3() {
        return cateset3;
    }

    @Override
    public void processCategoryShop(String output) {
        Log.d("API categorized_product", output);
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String image = jsonObject.getString("image");
                String name = jsonObject.getString("name");
                String category_product_name = jsonObject.getJSONObject("category_product").getString("name");
                int productId = jsonObject.getInt("id");

                tvCategory3.setText(category_product_name);

                CateproModel3 item = new CateproModel3(name, image, productId);
                cateset3.add(item);
            }

            mLayoutManager4 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            mRecyclerView4.setLayoutManager(mLayoutManager4);
            mAdapter4 = new CateproAdapter3(getActivity(), initCate3());
            mRecyclerView4.setAdapter(mAdapter4);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
