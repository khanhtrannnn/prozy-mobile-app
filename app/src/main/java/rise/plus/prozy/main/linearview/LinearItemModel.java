package rise.plus.prozy.main.linearview;


public class LinearItemModel {

    private String name;
    private String image;
    private double price;
    private int productId;

    public LinearItemModel(String name, String image, double price, int productId) {
        this.name = name;
        this.image = image;
        this.price = price;
        this.productId = productId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
