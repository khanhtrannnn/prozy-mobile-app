package rise.plus.prozy.main.category;


public class CateproModel {

    private String name;
    private String image;
    private int productId;

    public CateproModel(String name, String image, int productId) {
        this.name = name;
        this.image = image;
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}
