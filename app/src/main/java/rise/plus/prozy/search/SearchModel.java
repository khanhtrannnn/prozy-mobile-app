package rise.plus.prozy.search;

public class SearchModel {

    private int shopId;
    private int productId;
    private int userIdProduct;
    private String name;
    private Double price;
    private float rating;
    private String imgUrl;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public SearchModel(int productId, int shopId, int userIdProduct, String name, Double price, float rating, String imgUrl) {
        this.shopId = shopId;
        this.productId = productId;
        this.userIdProduct = userIdProduct;
        this.name = name;
        this.imgUrl = imgUrl;
        this.price = price;
        this.rating = rating;
    }

    public int getUserIdProduct() {
        return userIdProduct;
    }

    public void setUserIdProduct(int userIdProduct) {
        this.userIdProduct = userIdProduct;
    }
}
