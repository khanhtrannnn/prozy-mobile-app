package rise.plus.prozy.search;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.BaseActivity;
import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;
import rise.plus.prozy.product.ProductActivity;
import rise.plus.prozy.services.CategoryShopGet;
import rise.plus.prozy.services.ShopGet;
import rise.plus.prozy.services.async_response.CategoryProductResponse;
import rise.plus.prozy.services.async_response.CategoryShopResponse;
import rise.plus.prozy.services.async_response.CommentProductResponse;
import rise.plus.prozy.services.async_response.ProductResponse;
import rise.plus.prozy.services.CategoryProductGet;
import rise.plus.prozy.services.ProductGet;
import rise.plus.prozy.services.async_response.ShopResponse;
import rise.plus.prozy.shop.ShopActivity;

import static rise.plus.prozy.Constant.UID;


public class SearchActivity extends BaseActivity implements AdapterView.OnItemSelectedListener,
        ProductResponse, ShopResponse, CategoryProductResponse,
        CategoryShopResponse, CommentProductResponse, View.OnClickListener {

    private RadioGroup radioGroup;
    private RadioButton radioButtonProduct, radioButtonShop;
    private String keyword = "", name, img;
    private int spinnerItemPosition, productId, shopId, productIdItemClicked, userIdProduct;
    private Double price;
    private RecyclerView myRecyclerView;
    private Spinner spinner;
    private EditText searchBox;
    private SearchModel item;
    private RecyclerView.Adapter myAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private float averageRatingg;
    private int userId = 0;
    private ImageView ivBack;
    private TextView tvSearchByProduct, tvSearchByCategory, mCount;
    List<SearchModel> dataset = new ArrayList<SearchModel>();
    List<String> categories = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        if (UID != null) {
            userId = Integer.parseInt(UID);
        }

        init();
        recyclerView();
        showToolBar(toolbar, this);
        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_image));
        navigationView();
        search();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Diodrum Medium.otf");
        tvSearchByCategory.setTypeface(typeface);
        tvSearchByProduct.setTypeface(typeface);
        radioButtonProduct.setTypeface(typeface);
        radioButtonShop.setTypeface(typeface);

        categories.add("All");
        new CategoryProductGet(this, "/category_products.json").execute();

        searchBox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        Drawable drawable = searchBox.getBackground();
        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        if (Build.VERSION.SDK_INT > 16) {
            searchBox.setBackground(drawable);
        } else {
            searchBox.setBackgroundDrawable(drawable);
        }
    }

    private void init() {
        ivBack = (ImageView) findViewById(R.id.iv_back);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioButtonProduct = (RadioButton) findViewById(R.id.rb_product);
        radioButtonShop = (RadioButton) findViewById(R.id.rb_shop);
        searchBox = (EditText) findViewById(R.id.search_box);
        myRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_search_list);
        spinner = (Spinner) findViewById(R.id.spinner);
        tvSearchByProduct = (TextView) findViewById(R.id.search_by_product);
        tvSearchByCategory = (TextView) findViewById(R.id.search_by_category);
        mCount = (TextView) findViewById(R.id.count);

        spinner.setOnItemSelectedListener(this);
        radioButtonProduct.setOnClickListener(this);
        radioButtonShop.setOnClickListener(this);
        radioButtonProduct.setChecked(true);
        searchBox.setOnClickListener(this);
        ivBack.setOnClickListener(this);

    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        String item = parent.getItemAtPosition(position).toString();
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();
        spinnerItemPosition = spinner.getSelectedItemPosition();

        Log.d("Spinner item position", String.valueOf(spinnerItemPosition));
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rb_product:
                clearList();
                new CategoryProductGet(this, "/category_products.json").execute();
                break;
            case R.id.rb_shop:
                clearList();
                new CategoryShopGet(this, "/category_shops.json").execute();
                break;
            case R.id.iv_back:
                finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        return true;
    }

    private void search() {
        keyword = searchBox.getText().toString();

        Log.d("testClickSearch", "click");

        if (radioButtonProduct.isChecked() && spinnerItemPosition == 0) {
            new ProductGet(SearchActivity.this, "/products.json?keyword=" + keyword).execute();

        } else if (radioButtonShop.isChecked() && spinnerItemPosition == 0) {
            new ShopGet(SearchActivity.this, "/shops.json?keyword=" + keyword).execute();

        } else if (radioButtonProduct.isChecked() && spinnerItemPosition != 0) {
            new ProductGet(SearchActivity.this, "/products.json?keyword=" + keyword +
                    "&category_product_id=" + spinnerItemPosition).execute();

        } else if (radioButtonShop.isChecked() && spinnerItemPosition != 0) {
            new ShopGet(SearchActivity.this, "/shops.json?keyword=" + keyword +
                    "&category_shop_id=" + spinnerItemPosition).execute();
        }
    }

    public void recyclerView() {

        myRecyclerView.setHasFixedSize(true);
        myRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, myRecyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (radioButtonProduct.isChecked()) {
                            productIdItemClicked = dataset.get(position).getProductId();

                            Log.d("Product-id", String.valueOf(productIdItemClicked) + " at position "
                                    + String.valueOf(position));

                            Intent intent = new Intent(SearchActivity.this, ProductActivity.class);
                            intent.putExtra("product-id-item-clicked", productIdItemClicked);
                            startActivity(intent);
                        } else if (radioButtonShop.isChecked()) {
                            int ShopIdItemClicked = dataset.get(position).getShopId();

                            Log.d("Shop-id", String.valueOf(ShopIdItemClicked) + " at position "
                                    + String.valueOf(position));

                            Intent intent2 = new Intent(SearchActivity.this, ShopActivity.class);
                            intent2.putExtra("shop-id-item-clicked", ShopIdItemClicked);
                            startActivity(intent2);
                        }

//                        SearchModel item = new SearchModel(productId, shopId, name, price, 1, img);
//                        dataset.add(item);

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));

    }

    private List<SearchModel> initIndex() {
        return dataset;
    }

    @Override
    public void processProduct(String output) {
        Log.d("API product", output);
        dataset.clear();
        try {
            JSONArray jsonArray = new JSONArray(output);

            if (jsonArray.length() == 0) {
                Toast.makeText(this, "No results found", Toast.LENGTH_SHORT).show();
            }

            int countReview = jsonArray.length();
            if (countReview == 1) {
                mCount.setText(String.valueOf(countReview) + " result");
            } else if (countReview > 1) {
                mCount.setText(String.valueOf(countReview) + " results");
            }

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                name = jsonObj.getString("name");
                price = jsonObj.getDouble("price");
                img = jsonObj.getString("image");
                productId = jsonObj.getInt("id");
                userIdProduct = jsonObj.getJSONObject("user").getInt("id");

                item = new SearchModel(productId, shopId, userIdProduct, name, price, 3, img);
                dataset.add(item);

            }

            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            myRecyclerView.setLayoutManager(mLayoutManager);
            myAdapter = new SearchAdapter(SearchActivity.this, initIndex());
            myRecyclerView.setAdapter(myAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }

    }

    @Override
    public void processShop(String output) {
        Log.d("API shop", output);
        dataset.clear();
        try {
            JSONArray jsonArray = new JSONArray(output);

            if (jsonArray.length() == 0) {
                Toast.makeText(this, "No results found", Toast.LENGTH_SHORT).show();
            }

            int countReview = jsonArray.length();
            if (countReview == 1) {
                mCount.setText(String.valueOf(countReview) + " result");
            } else if (countReview > 1) {
                mCount.setText(String.valueOf(countReview) + " results");
            }

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = (JSONObject) jsonArray.get(i);
                name = jsonObj.getString("name");
                img = "";
                shopId = jsonObj.getInt("id");
                Log.d("shop_id", String.valueOf(shopId));

                item = new SearchModel(productId, shopId, userIdProduct, name, -1.0, 5, img);
                dataset.add(item);
            }

            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            myRecyclerView.setLayoutManager(mLayoutManager);
            myAdapter = new SearchAdapter(SearchActivity.this, initIndex());
            myRecyclerView.setAdapter(myAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }
    }


    private void clearList() {
        dataset.clear();
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecyclerView.setLayoutManager(mLayoutManager);
        myAdapter = new SearchAdapter(SearchActivity.this, initIndex());
        myRecyclerView.setAdapter(myAdapter);
    }


    @Override
    public void processCategoryProduct(String output) {

        try {
            JSONArray jsonArray = new JSONArray(output);
            Log.d("API category_products", output);

            categories.clear();
            categories.add("All");
            Spinner spinner = (Spinner) findViewById(R.id.spinner);
            spinner.setOnItemSelectedListener(this);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String name = mainObject.getString("name");

                categories.add(name);

            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }
    }


    @Override
    public void processCategoryShop(String output) {
        try {
            JSONArray jsonArray = new JSONArray(output);
            Log.d("API category_shops", output);

            categories.clear();
            categories.add("All");


            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String name = mainObject.getString("name");

                categories.add(name);

            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }
    }

    @Override
    public void processCommentProduct(String output) {
        Log.d("API comment_products", output);
        int count = 0;

        try {
            JSONArray jsonArray = new JSONArray(output);
            float sumRating = 0;
            int productId = 0;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                int rating = mainObject.getInt("rating");
                int userIdComment = mainObject.getJSONObject("user").getInt("id");
                productId = mainObject.getJSONObject("product").getInt("id");
                if (userIdComment != userIdProduct) {
                    sumRating += rating;
                    count++;
                }
            }

            averageRatingg = sumRating / count;

            Log.d("Average rating product", String.valueOf(averageRatingg));

            mLayoutManager = new LinearLayoutManager(getApplicationContext());
            myRecyclerView.setLayoutManager(mLayoutManager);
            myAdapter = new SearchAdapter(SearchActivity.this, initIndex());
            myRecyclerView.setAdapter(myAdapter);

        } catch (Exception e) {

        }
    }


}
