package rise.plus.prozy.search;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import rise.plus.prozy.R;

import static rise.plus.prozy.Constant.host;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private List<SearchModel> mIndices;
    private Context mContext;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvPrice;
        public ImageView ivImage;

        public ViewHolder(View v) {
            super(v);
            tvName = (TextView) v.findViewById(R.id.search_name);
            tvPrice = (TextView) v.findViewById(R.id.search_price);
            ivImage = (ImageView) v.findViewById(R.id.search_image);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
            tvName.setTypeface(typeface);
        }
    }

    public SearchAdapter(Context context, List<SearchModel> dataset) {
        mIndices = dataset;
        mContext = context;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.recycler_list_search, parent, false);

//        LinearLayout search_result = (LinearLayout) v.findViewById(R.id.ll_search);
//        search_result.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        SearchModel index = mIndices.get(position);

        holder.tvName.setText(index.getName());
        holder.tvPrice.setText("$" + index.getPrice().toString());

//        Log.d("rating myadapter", String.valueOf(index.getRating()));

        if (index.getPrice() == -1.0) {
            holder.tvPrice.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.GONE);
        } else {

//            Log.d("shop", String.valueOf(index.getImgUrl()));
//            holder.tvPrice.setText("");

//            String imgUrl = host + index.getImgUrl() +"";
            Glide.with(mContext).load(host + index.getImgUrl()).into(holder.ivImage);
        }

    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}
