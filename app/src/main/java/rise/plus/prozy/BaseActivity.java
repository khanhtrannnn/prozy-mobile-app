package rise.plus.prozy;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import rise.plus.prozy.authorization.LoginActivity;
import rise.plus.prozy.navigation_drawer.ProfileActivity;
import rise.plus.prozy.product.AddProductActivity;
import rise.plus.prozy.search.SearchActivity;
import rise.plus.prozy.services.UserGet;
import rise.plus.prozy.services.async_response.UserResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.host;

public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UserResponse {

    public Context mContext;
    public Toolbar toolbar;
    public TextView navName, navEmail, navText;
    public CircleImageView navAvatar;
    public Button navBtnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = BaseActivity.this;

        new UserGet(this, "/users/" + UID + ".json").execute();

    }

    public void showToolBar(Toolbar toolbar, final AppCompatActivity activity) {
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        @SuppressWarnings("deprecation")
        Drawable drawable = activity.getResources().getDrawable(
                R.drawable.prozy_logo);
        drawable.setColorFilter(
                activity.getResources().getColor(R.color.colorWhite),
                android.graphics.PorterDuff.Mode.SRC_ATOP);
        activity.getSupportActionBar().setHomeAsUpIndicator(drawable);
        toolbar.setBackgroundColor(activity.getResources().getColor(
                R.color.colorPrimary));
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView prozyLogo = (ImageView) findViewById(R.id.toolbar_logo);
        prozyLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ProzyActivity.class);
                startActivity(intent);
            }
        });
    }

    public void navigationView() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
//        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu navMenu = navigationView.getMenu();

        if (UID != null) {
            navMenu.findItem(R.id.nav_profile).setVisible(true);
            navMenu.findItem(R.id.nav_logout).setVisible(true);
            navMenu.findItem(R.id.nav_favorite).setVisible(true);
            navMenu.findItem(R.id.nav_home).setVisible(true);

        } else {
            navMenu.findItem(R.id.nav_profile).setVisible(false);
            navMenu.findItem(R.id.nav_logout).setVisible(false);
            navMenu.findItem(R.id.nav_favorite).setVisible(false);
            navMenu.findItem(R.id.nav_home).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        navName = (TextView) headerView.findViewById(R.id.nav_name);
        navEmail = (TextView) headerView.findViewById(R.id.nav_email);
        navText = (TextView) headerView.findViewById(R.id.nav_text);
        navAvatar = (CircleImageView) headerView.findViewById(R.id.nav_profile_picture);
        navAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        navBtnLogin = (Button) headerView.findViewById(R.id.nav_login);
        navBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        if (UID != null) {
            navBtnLogin.setVisibility(View.GONE);
            navText.setVisibility(View.GONE);
        } else {
        }

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/AmsiPro Regular.ttf");
        navText.setTypeface(typeface);

        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "fonts/Diodrum Medium.otf");
        navName.setTypeface(typeface2);
        navEmail.setTypeface(typeface2);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_add_product) {
            Intent intent = new Intent(this, AddProductActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.home) {
            Intent intent = new Intent(this, ProzyActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            Logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void processUser(String output) {
        Log.d("APIusernav", output);

        try {
            JSONObject mainObject = new JSONObject(output);
            String name = mainObject.getString("name");
            String email = mainObject.getString("email");
            String avatar = mainObject.getString("avatar");

            Glide.with(this).load(host + avatar).into(navAvatar);
            navName.setText("Hello, " + name + " !");
            navEmail.setText(email);

        } catch (Exception e) {

        }
    }

    private void Logout() {
        UID = null;
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

}

