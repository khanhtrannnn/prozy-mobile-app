package rise.plus.prozy.authorization;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import rise.plus.prozy.ProzyActivity;
import rise.plus.prozy.R;

import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.services.ServicePost;

import static rise.plus.prozy.Constant.UID;

public class LoginActivity extends ProzyActivity implements View.OnClickListener, PostResponse {

    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private EditText edtUsername, edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/AmsiPro Regular.ttf");
        edtUsername.setTypeface(typeface);
        edtPassword.setTypeface(typeface);
    }

    private void init() {
        findViewById(R.id.btnlogin).setOnClickListener(this);
        findViewById(R.id.signUp).setOnClickListener(this);
        findViewById(R.id.icon_login).setOnClickListener(this);

        edtUsername = (EditText) findViewById(R.id.username);
        edtPassword = (EditText) findViewById(R.id.password);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnlogin:
                logIn();
                break;
            case R.id.signUp:
                signUp();
                break;
            case R.id.icon_login:
                goToMainPage();
                break;
        }

    }

    private void signUp() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void goToMainPage() {
        Intent intent = new Intent(this, ProzyActivity.class);
        startActivity(intent);
    }

    private void logIn() {
        String username = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();

        String json = "{   \"username\":    \"" + username + "\"  ,   \"password\"  :  \"" + password + "\"   }";

        new ServicePost(this, "/users.json", json).execute();

    }

    private void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
//        loginButton = (LoginButton) findViewById(R.id.login_button);
//        loginButton.setReadPermissions("email");
//        loginButton.setReadPermissions("user_friends");
//        loginButton.setReadPermissions("public_profile");
//        loginButton.setReadPermissions("user_birthday");
        Log.d("Test log", "log");

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("Test facebook", "success");
                Log.d("Test facebook", loginResult.getAccessToken().getToken() + "");
                Log.d("Test facebook", loginResult.getAccessToken().getUserId() + "");
                Log.d("Test facebook", loginResult.getRecentlyDeniedPermissions() + "");

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());

                                // Application code
                                try {
                                    Log.d("facebook", object + "");
                                    String birthday = object.getString("birthday");
                                    String email = object.getString("email");
//                                    Log.d("facebook",birthday + " " + email);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                // 01/31/1980 format
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        Log.d("test", "success");
    }

    @Override
    public void MainPage() {
    }

    @Override
    public void processPost(String output) {
        Log.d("json", output);

        try {
            JSONObject jsonObject = new JSONObject(output);
            UID = String.valueOf(jsonObject.getInt("id"));

            Intent intent = new Intent(this, ProzyActivity.class);
            startActivity(intent);

        } catch (Exception e) {

        }
    }
}
