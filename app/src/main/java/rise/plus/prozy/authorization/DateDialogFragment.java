package rise.plus.prozy.authorization;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DateDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    String setDate;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //        // Use the current date as the default date in the picker
        final Calendar c;
        int year = 0;
        int month = 0;
        int day = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            c = Calendar.getInstance();
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);

        //
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        setDate = dayOfMonth + "-" + (month + 1) + "-" + year;

    }
}
