package rise.plus.prozy.authorization;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import rise.plus.prozy.R;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.async_response.PostResponse;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, PostResponse {
    Button createUser;
    EditText edtUsername, edtPassword, edtName, edtLastName, edtEmail, edtConfirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();

        createUser = (Button) findViewById(R.id.create_user);
        createUser.setOnClickListener(this);

//        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/AmsiPro Regular.ttf");
//        edtUsername.setTypeface(typeface);
//        edtPassword.setTypeface(typeface);
//        edtName.setTypeface(typeface);
//        edtLastName.setTypeface(typeface);
//        edtEmail.setTypeface(typeface);
//        edtConfirmPassword.setTypeface(typeface);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.create_user:
                register();
                break;
            case R.id.back_login:
                backToLogin();
                break;
        }
    }

    public void init() {
        edtUsername = (EditText) findViewById(R.id.username);
        edtPassword = (EditText) findViewById(R.id.password);
        edtConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        edtName = (EditText) findViewById(R.id.name);
        edtLastName = (EditText) findViewById(R.id.lastname);
        edtEmail = (EditText) findViewById(R.id.email);
        findViewById(R.id.back_login).setOnClickListener(this);

    }

    private void backToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void register() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_create_user, null);
        builder.setView(view);

        final EditText edtUsername = (EditText) findViewById(R.id.username);
        final EditText edtPassword = (EditText) findViewById(R.id.password);
        final EditText edtName = (EditText) findViewById(R.id.name);
        final EditText edtLastName = (EditText) findViewById(R.id.lastname);
        final EditText edtEmail = (EditText) findViewById(R.id.email);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                String name = edtName.getText().toString();
                String lastname = edtLastName.getText().toString();
                String email = edtEmail.getText().toString();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("username", username);
                    jsonObject.put("password", password);
                    jsonObject.put("name", name);
                    jsonObject.put("lastname", lastname);
                    jsonObject.put("email", email);
                    jsonObject.put("role_id", 2);
                    jsonObject.put("avatar", "abc");

                    Log.d("createuser", String.valueOf(jsonObject));

                    new ServicePost(RegisterActivity.this, "/users.json", String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    @Override
    public void processPost(String output) {
        Toast.makeText(this, "Create account successfully", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
