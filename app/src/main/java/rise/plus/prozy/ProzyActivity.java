package rise.plus.prozy;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import rise.plus.prozy.main.MainPage;

public class ProzyActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prozy);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        showToolBar(toolbar, this);

        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.toolbar_image));

        navigationView();
        MainPage();
    }

    public void MainPage() {
        MainPage fragment = new MainPage();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(
                R.id.content_main_fragment,
                fragment,
                fragment.getTag()
        ).commit();
    }

}

