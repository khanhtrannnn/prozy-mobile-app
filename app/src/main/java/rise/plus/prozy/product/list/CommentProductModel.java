package rise.plus.prozy.product.list;

public class CommentProductModel {

    String message;
    int rating;
    String date;
    String user;
    int commentProductId;
    int userIdCommentProduct;
    int userIdProduct;

    public CommentProductModel(String message, int rating, String date, String user, int commentProductId, int userIdCommentProduct, int userIdProduct) {
        this.message = message;
        this.rating = rating;
        this.date = date;
        this.user = user;
        this.commentProductId = commentProductId;
        this.userIdCommentProduct = userIdCommentProduct;
        this.userIdProduct = userIdProduct;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getCommentProductId() {
        return commentProductId;
    }

    public void setCommentProductId(int commentProductId) {
        this.commentProductId = commentProductId;
    }

    public int getUserIdCommentProduct() {
        return userIdCommentProduct;
    }

    public void setUserIdCommentProduct(int userIdCommentProduct) {
        this.userIdCommentProduct = userIdCommentProduct;
    }

    public int getUserIdProduct() {
        return userIdProduct;
    }

    public void setUserIdProduct(int userIdProduct) {
        this.userIdProduct = userIdProduct;
    }
}
