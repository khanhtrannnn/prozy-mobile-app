package rise.plus.prozy.product.list;

public class ReviewProductModel {
    String title;
    String message;
    int rating;
    String date;
    String user;
    int reviewProductId;
    int userIdReviewProduct;
    int userIdProduct;

    public ReviewProductModel(String title, String message, int rating, String date, String user, int reviewProductId, int userIdReviewProduct, int userIdProduct) {
        this.title = title;
        this.message = message;
        this.rating = rating;
        this.date = date;
        this.user = user;
        this.reviewProductId = reviewProductId;
        this.userIdReviewProduct = userIdReviewProduct;
        this.userIdProduct = userIdProduct;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getReviewProductId() {
        return reviewProductId;
    }

    public void setReviewProductId(int reviewProductId) {
        this.reviewProductId = reviewProductId;
    }

    public int getUserIdReviewProduct() {
        return userIdReviewProduct;
    }

    public void setUserIdReviewProduct(int userIdReviewProduct) {
        this.userIdReviewProduct = userIdReviewProduct;
    }

    public int getUserIdProduct() {
        return userIdProduct;
    }

    public void setUserIdProduct(int userIdProduct) {
        this.userIdProduct = userIdProduct;
    }
}
