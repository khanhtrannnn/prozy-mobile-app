package rise.plus.prozy.product.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.product.list.ReviewProductAdapter;
import rise.plus.prozy.product.list.ReviewProductModel;
import rise.plus.prozy.product.list.RecyclerItemClickListener;
import rise.plus.prozy.review.DetailReviewProductActivity;
import rise.plus.prozy.services.ReviewProductGet;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.services.async_response.ReviewProductResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.userIdProduct;
import static rise.plus.prozy.Constant.userIdLogged;

public class ReviewProductFragment extends Fragment implements View.OnClickListener,
        ReviewProductResponse, PostResponse {

    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<ReviewProductModel> dataset = new ArrayList<ReviewProductModel>();
    private int productId, reviewProductIdItemClicked;
    private TextView mCount;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_review_product, container, false);
        init(view);
        recyclerView();

        Bundle bundle = getActivity().getIntent().getExtras();
        productId = bundle.getInt("product-id-item-clicked");

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }

        new ReviewProductGet(this, "/review_products.json?product_id=" + productId).execute();


        return view;

    }

    private void init(View view) {
        view.findViewById(R.id.btn_review).setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_review_product);
        mCount = (TextView) view.findViewById(R.id.count);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_review:
                review();
                break;
        }
    }

    private void review() {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_review_post, null);
        builder.setView(view);

        final EditText edtMessage = (EditText) view.findViewById(R.id.message);
        final EditText edtTitle = (EditText) view.findViewById(R.id.title);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

        if (userIdLogged == userIdProduct) {
            ratingBar.setVisibility(View.GONE);
        }

        builder.setPositiveButton("Review", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = edtTitle.getText().toString();
                String message = edtMessage.getText().toString();
                float rating = ratingBar.getRating();
                Date date = new Date();

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("title", title);
                    jsonObject.put("message", message);
                    jsonObject.put("rating", rating);
                    jsonObject.put("date", date);
                    jsonObject.put("user_id", UID);
                    jsonObject.put("product_id", productId);

                    Log.d("Review", String.valueOf(jsonObject));

                    new ServicePost(ReviewProductFragment.this, "/review_products.json",
                            String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }


    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                reviewProductIdItemClicked = dataset.get(position).getReviewProductId();
                Intent intent = new Intent(view.getContext(), DetailReviewProductActivity.class);
                intent.putExtra("review-product-id-item-clicked", reviewProductIdItemClicked);
                Log.d("Review-product-id", String.valueOf(reviewProductIdItemClicked));
                startActivity(intent);

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private List<ReviewProductModel> initIndex() {
        return dataset;
    }

    @Override
    public void processReviewProduct(String output) {
        Log.d("API_review_products", output);
        dataset.clear();
        try {
            JSONArray jsonArray = new JSONArray(output);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String username = mainObject.getJSONObject("user").getString("name");
                String title = mainObject.getString("title");
                String message = mainObject.getString("message");
                String date = mainObject.getString("date");
                int rating = mainObject.getInt("rating");
                int userIdReviewProduct = mainObject.getJSONObject("user").getInt("id");
                int reviewProductId = mainObject.getInt("id");

                int countReview = jsonArray.length();
                if (countReview == 1) {
                    mCount.setText(String.valueOf(countReview) + " review");
                } else if (countReview > 1) {
                    mCount.setText(String.valueOf(countReview) + " reviews");
                }

                Log.d("REVIEW PRODUCT", "user_id_product: " + String.valueOf(userIdProduct) + ", user_id_review: "
                        + userIdReviewProduct + ", user_id_logged: " + userIdLogged);

                ReviewProductModel item =
                        new ReviewProductModel(title, message, rating, date, username, reviewProductId, userIdReviewProduct, userIdProduct);
                dataset.add(item);
            }

            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            mAdapter = new ReviewProductAdapter(getActivity(), initIndex(), getActivity());
            mRecyclerView.setAdapter(mAdapter);

        } catch (Exception e) {

        }
    }

    @Override
    public void processPost(String output) {
        new ReviewProductGet(this, "/review_products?product_id=" + productId + ".json").execute();
    }

}

