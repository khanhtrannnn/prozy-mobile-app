package rise.plus.prozy.product.list;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.services.ServiceDelete;
import rise.plus.prozy.services.ServiceUpdate;
import rise.plus.prozy.services.async_response.DeleteResponse;
import rise.plus.prozy.services.async_response.UpdateResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.commentProductId;
import static rise.plus.prozy.Constant.productId;
import static rise.plus.prozy.Constant.userIdLogged;
import static rise.plus.prozy.Constant.userIdProduct;

public class CommentProductAdapter extends RecyclerView.Adapter<CommentProductAdapter.ViewHolder> {

    private List<CommentProductModel> mIndices;
    private static Context mContext;
    private static Activity activity;

    public static class ViewHolder extends RecyclerView.ViewHolder implements DeleteResponse, UpdateResponse, View.OnCreateContextMenuListener {

        public RatingBar rating;
        public TextView tvDate, tvMessage, tvUser;
        public ImageButton btnEdit, btnDelete;
        public ImageView mTick;

        public ViewHolder(View view) {
            super(view);

            tvMessage = (TextView) view.findViewById(R.id.comment_message);
            rating = (RatingBar) view.findViewById(R.id.comment_rating);
            tvDate = (TextView) view.findViewById(R.id.comment_date);
            tvUser = (TextView) view.findViewById(R.id.comment_user);
            mTick = (ImageView) view.findViewById(R.id.tick);
            btnEdit = (ImageButton) view.findViewById(R.id.btn_edit);
            btnDelete = (ImageButton) view.findViewById(R.id.btn_delete);
            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Semibold.otf");
            tvUser.setTypeface(typeface);
            view.setOnCreateContextMenuListener(this);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(v.getRootView().getContext());
                    LayoutInflater inflater = (LayoutInflater) mContext.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflater.inflate(R.layout.dialog_comment_delete, null);

                    builder.setView(view);

                    builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int commentProductId = (int) v.getTag();

                            //Toast.makeText(v.getContext(), Integer.toString(commentProductId), Toast.LENGTH_SHORT).show();
                            new ServiceDelete(ViewHolder.this, "/comment_products/" + commentProductId + ".json").execute();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();

                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(v.getRootView().getContext());
                    LayoutInflater inflater = (LayoutInflater) mContext.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflater.inflate(R.layout.dialog_comment_edit, null);

                    builder.setView(view);

                    final EditText edtMessage = (EditText) view.findViewById(R.id.message);
                    final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

                    if (userIdLogged == userIdProduct) {
                        ratingBar.setVisibility(View.GONE);
                    }

                    builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String message = edtMessage.getText().toString();
                            float rating = ratingBar.getRating();
                            Date date = new Date();
                            final int commentProductId = (int) v.getTag();

                            Log.d("COMMENT", " message: " + message + ", rating: " + rating + ", date: " + date);
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("message", message);
                                jsonObject.put("rating", rating);
                                jsonObject.put("date", date);
                                jsonObject.put("user_id", UID);
                                jsonObject.put("product_id", productId);

                                Log.d("Comment", String.valueOf(jsonObject));

                                new ServiceUpdate(ViewHolder.this,
                                        "/comment_products/" + commentProductId + ".json",
                                        String.valueOf(jsonObject)).execute();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();

                }
            });

        }


        @Override
        public void processUpdate(String output) {
            activity.finish();
            activity.startActivity(activity.getIntent());
        }

        @Override
        public void processDelete() {
            activity.finish();
            activity.startActivity(activity.getIntent());
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Edit");
            menu.add(0, v.getId(), 0, "Delete");
        }
    }


    public CommentProductAdapter(Context context, List<CommentProductModel> dataset, Activity activity) {
        mIndices = dataset;
        mContext = context;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_comment, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        CommentProductModel index = mIndices.get(position);
        viewHolder.btnDelete.setTag(index.getCommentProductId());
        viewHolder.btnEdit.setTag(index.getCommentProductId());

        viewHolder.tvDate.setText(index.getDate());
        viewHolder.tvMessage.setText(index.getMessage());
        viewHolder.rating.setRating(index.getRating());
        viewHolder.tvUser.setText(index.getUser());
        commentProductId = index.getCommentProductId();
        Log.d("COMMENT PRODUCT ID", String.valueOf(commentProductId));

        if (index.getUserIdCommentProduct() == index.getUserIdProduct()) {
            viewHolder.rating.setVisibility(View.GONE);
        }

        if (index.getUserIdCommentProduct() == userIdLogged) {
            viewHolder.btnEdit.setVisibility(View.VISIBLE);
            viewHolder.btnDelete.setVisibility(View.VISIBLE);

        } else {
            viewHolder.btnEdit.setVisibility(View.GONE);
            viewHolder.btnDelete.setVisibility(View.GONE);

        }

        if (index.getUserIdProduct() == index.getUserIdCommentProduct()) {
            viewHolder.mTick.setVisibility(View.VISIBLE);
        } else {
            viewHolder.mTick.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }


}