package rise.plus.prozy.product.list;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.services.ServiceDelete;
import rise.plus.prozy.services.ServiceUpdate;
import rise.plus.prozy.services.async_response.DeleteResponse;
import rise.plus.prozy.services.async_response.UpdateResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.reviewProductId;
import static rise.plus.prozy.Constant.productId;
import static rise.plus.prozy.Constant.userIdLogged;
import static rise.plus.prozy.Constant.userIdProduct;

public class ReviewProductAdapter extends RecyclerView.Adapter<ReviewProductAdapter.ViewHolder> {

    private List<ReviewProductModel> mIndices;
    private static Context mContext;
    private static Activity activity;

    public static class ViewHolder extends RecyclerView.ViewHolder implements DeleteResponse, UpdateResponse {
        public TextView tvTitle, tvDate, tvMessage, tvUser;
        public RatingBar rating;
        public ImageButton btnEdit, btnDelete;

        public ViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.review_title);
            tvMessage = (TextView) view.findViewById(R.id.review_message);
            rating = (RatingBar) view.findViewById(R.id.review_rating);
            tvDate = (TextView) view.findViewById(R.id.review_date);
            tvUser = (TextView) view.findViewById(R.id.review_user);
            btnEdit = (ImageButton) view.findViewById(R.id.btn_edit);
            btnDelete = (ImageButton) view.findViewById(R.id.btn_delete);

            Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Semibold.otf");
            tvUser.setTypeface(typeface);
//            Typeface typeface2 = Typeface.createFromAsset(mContext.getAssets(), "fonts/Diodrum Medium.otf");
//            tvMessage.setTypeface(typeface2);
//            Typeface typeface3 = Typeface.createFromAsset(mContext.getAssets(), "fonts/UTM Alexander.ttf");
//            tvTitle.setTypeface(typeface3);

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(v.getRootView().getContext());
                    LayoutInflater inflater = (LayoutInflater) mContext.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflater.inflate(R.layout.dialog_review_delete, null);

                    builder.setView(view);

                    builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int reviewProductId = (int) v.getTag();

                            new ServiceDelete(ViewHolder.this, "/review_products/" + reviewProductId + ".json").execute();
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();

                }
            });

            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(v.getRootView().getContext());
                    LayoutInflater inflater = (LayoutInflater) mContext.
                            getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View view = inflater.inflate(R.layout.dialog_review_edit, null);

                    builder.setView(view);

                    final EditText edtTitle = (EditText) view.findViewById(R.id.title);
                    final EditText edtMessage = (EditText) view.findViewById(R.id.message);
                    final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

                    if (userIdLogged == userIdProduct) {
                        ratingBar.setVisibility(View.GONE);
                    }

                    builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String title = edtTitle.getText().toString();
                            String message = edtMessage.getText().toString();
                            float rating = ratingBar.getRating();
                            Date date = new Date();
                            final int reviewProductId = (int) v.getTag();

                            Log.d("REVIEW", " title: " + title + ", message: " + message + ", rating: " + rating);
                            JSONObject jsonObject = new JSONObject();

                            try {
                                jsonObject.put("title", title);
                                jsonObject.put("message", message);
                                jsonObject.put("rating", rating);
                                jsonObject.put("date", date);
                                jsonObject.put("user_id", UID);
                                jsonObject.put("product_id", productId);

                                Log.d("Review", String.valueOf(jsonObject));

                                new ServiceUpdate(ViewHolder.this,
                                        "/review_products/" + reviewProductId + ".json",
                                        String.valueOf(jsonObject)).execute();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });

        }

        @Override
        public void processUpdate(String output) {
            activity.finish();
            activity.startActivity(activity.getIntent());
        }

        @Override
        public void processDelete() {
            activity.finish();
            activity.startActivity(activity.getIntent());
        }
    }


    public ReviewProductAdapter(Context context, List<ReviewProductModel> dataset, Activity activity) {
        mIndices = dataset;
        mContext = context;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.recycler_review, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        ReviewProductModel index = mIndices.get(position);
        viewHolder.btnDelete.setTag(index.getReviewProductId());
        viewHolder.btnEdit.setTag(index.getReviewProductId());

        viewHolder.tvDate.setText(index.getDate());
        viewHolder.tvMessage.setText(Html.fromHtml(index.getMessage()));
        viewHolder.rating.setRating(index.getRating());
        viewHolder.tvUser.setText(index.getUser());
        viewHolder.tvTitle.setText(index.getTitle());

        reviewProductId = index.getReviewProductId();

        if (index.getUserIdReviewProduct() == index.getUserIdProduct()) {
            viewHolder.rating.setVisibility(View.GONE);
        }

        if (index.getUserIdReviewProduct() == index.getUserIdProduct()) {
            viewHolder.rating.setVisibility(View.GONE);
        }
        if (index.getUserIdReviewProduct() == userIdLogged) {
            viewHolder.btnEdit.setVisibility(View.VISIBLE);
            viewHolder.btnDelete.setVisibility(View.VISIBLE);
        } else {
            viewHolder.btnEdit.setVisibility(View.GONE);
            viewHolder.btnDelete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mIndices.size();
    }
}