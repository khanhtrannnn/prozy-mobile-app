package rise.plus.prozy.product;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.services.CategoryProductGet;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.ShopGet;
import rise.plus.prozy.services.async_response.CategoryProductResponse;
import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.services.async_response.ShopResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.userIdLogged;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemSelectedListener, CategoryProductResponse, ShopResponse, PostResponse {

    ImageView productImage;
    private static final int PICK_PHOTO_FOR_AVATAR = 1000;
    List<String> categories = new ArrayList<String>();
    private Spinner spinner;
    private int categoryId, shopId;
    private String imageBase64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }

        spinner = (Spinner) findViewById(R.id.category_spinner);
        Button createButton = (Button) findViewById(R.id.btn_create_product);
        productImage = (ImageView) findViewById(R.id.product_image);
        createButton.setOnClickListener(this);
        productImage.setOnClickListener(this);

        new CategoryProductGet(this, "/category_products.json").execute();
        new ShopGet(this, "/shops.json?user_id=" + userIdLogged).execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_product:
                create();
                break;
            case R.id.product_image:
                pickImage();
                break;
        }
    }

    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            try {

                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                imageBase64 = Base64.encodeToString(byteArray, 0);

                Drawable drawable = new BitmapDrawable(getResources(), bitmap);
                Log.d("Test image", String.valueOf(inputStream));

                productImage.setImageDrawable(drawable);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        categoryId = spinner.getSelectedItemPosition() + 1;
        Log.d("category-id", String.valueOf(categoryId));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void create() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_create_product, null);
        builder.setView(view);

        final EditText edtProductName = (EditText) findViewById(R.id.product_name);
        final EditText edtPrice = (EditText) findViewById(R.id.product_price);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = edtProductName.getText().toString();
                float price = Float.valueOf(edtPrice.getText().toString());
                Log.d("Post_product", " name: " + name + ", price: " + price + ", category_product_id: " + categoryId);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", name);
                    jsonObject.put("price", price);
                    jsonObject.put("category_product_id", categoryId);
                    jsonObject.put("shop_id", shopId);
                    jsonObject.put("user_id", userIdLogged);
                    jsonObject.put("image", imageBase64);
                    Log.d("Post_product", imageBase64);
                    Log.d("Post_product", String.valueOf(jsonObject));

                    new ServicePost(AddProductActivity.this, "/products.json", String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    @Override
    public void processCategoryProduct(String output) {

        try {
            JSONArray jsonArray = new JSONArray(output);
            Log.d("API category_products", output);

            spinner.setOnItemSelectedListener(this);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String name = jsonObject.getString("name");

                categories.add(name);

            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }
    }

    @Override
    public void processShop(String output) {
        Log.d("API shop", output);

        try {
            JSONObject jsonObject = new JSONObject(output);
            shopId = jsonObject.getInt("id");

        } catch (Exception e) {

        }
    }

    @Override
    public void processPost(String output) {

    }
}
