package rise.plus.prozy.product.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.services.ServicePost;
import rise.plus.prozy.services.async_response.CommentProductResponse;
import rise.plus.prozy.services.async_response.ProductResponse;
import rise.plus.prozy.services.ProductGet;
import rise.plus.prozy.product.list.CommentProductAdapter;
import rise.plus.prozy.product.list.RecyclerItemClickListener;
import rise.plus.prozy.product.list.CommentProductModel;
import rise.plus.prozy.services.CommentProductGet;
import rise.plus.prozy.services.async_response.PostResponse;
import rise.plus.prozy.shop.ShopActivity;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.host;
import static rise.plus.prozy.Constant.userIdLogged;
import static rise.plus.prozy.Constant.userIdProduct;
import static rise.plus.prozy.Constant.productId;


public class DetailProductFragment extends Fragment
        implements View.OnClickListener, ProductResponse, CommentProductResponse, PostResponse {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private View view;
    private TextView tvName, tvPrice, tvInformation, tvComment, tvBy, tvShopName;
    private ImageView ivProductImage;
    private RatingBar ratingBar;
    private List<CommentProductModel> dataset = new ArrayList<CommentProductModel>();
    private float averageRating;
    private int shopId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_detail_product, container, false);
        init(view);
        recyclerView();
        textStyle();

        Bundle bundle = getActivity().getIntent().getExtras();
        productId = bundle.getInt("product-id-item-clicked");

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }

        new ProductGet(this, "/products/" + productId + ".json").execute();
        new CommentProductGet(this, "/comment_products?product_id=" + productId + ".json").execute();

        return view;
    }

    private void init(View view) {

        view.findViewById(R.id.btn_comment).setOnClickListener(this);
        view.findViewById(R.id.shop).setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_comment_product);
        tvName = (TextView) view.findViewById(R.id.name);
        ivProductImage = (ImageView) view.findViewById(R.id.image);
        tvPrice = (TextView) view.findViewById(R.id.price);
        ratingBar = (RatingBar) view.findViewById(R.id.rating_bar);
        tvShopName = (TextView) view.findViewById(R.id.shop_name);
        tvInformation = (TextView) view.findViewById(R.id.infomation);
        tvComment = (TextView) view.findViewById(R.id.comment);
        tvBy = (TextView) view.findViewById(R.id.by);

    }

    private void textStyle() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Diodrum Medium.otf");
        tvBy.setTypeface(typeface);
        tvShopName.setTypeface(typeface);

        Typeface typeface2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Diodrum Semibold.otf");
//        tvInformation.setTypeface(typeface2);
//        tvComment.setTypeface(typeface2);
        tvName.setTypeface(typeface2);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_comment:
                comment();
                break;
            case R.id.shop:
                shop();
                break;
        }
    }

    private void recyclerView() {

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener
                (getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }

                    @Override
                    public void onItemLongClick(View view, int position) {

                    }
                }));
    }

    private void shop() {
        Intent intent2 = new Intent(getActivity().getApplication(), ShopActivity.class);
        intent2.putExtra("shop-id-item-clicked", shopId);
        startActivity(intent2);
    }

    private void comment() {

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_comment_post, null);
        builder.setView(view);

        final EditText edtMessage = (EditText) view.findViewById(R.id.message);
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.rating);

        if (userIdLogged == userIdProduct) {
            ratingBar.setVisibility(View.GONE);
        }

        builder.setPositiveButton("Comment", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String message = edtMessage.getText().toString();
                float rating = ratingBar.getRating();
                Date date = new Date();

                Log.d("COMMENT", " message: " + message + ", rating: " + rating);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("message", message);
                    jsonObject.put("rating", rating);
                    jsonObject.put("date", date);
                    jsonObject.put("user_id", UID);
                    jsonObject.put("product_id", productId);

                    Log.d("Comment", String.valueOf(jsonObject));

                    new ServicePost(DetailProductFragment.this, "/comment_products.json",
                            String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    @Override
    public void processProduct(String output) {
        Log.d("API product", output);

        try {
            JSONObject mainObject = new JSONObject(output);
            String name = mainObject.getString("name");
            String price = mainObject.getString("price");
            String image = mainObject.getString("image");
            String shopName = mainObject.getJSONObject("shop").getString("name");
            userIdProduct = mainObject.getInt("user_id");
            shopId = mainObject.getJSONObject("shop").getInt("id");

            tvName.setText(name);
            tvName.setText(name);
            Glide.with(this).load(host + image).into(ivProductImage);
            tvPrice.setText(price);

            tvShopName.setText(shopName);

        } catch (Exception e) {

        }
    }

    private List<CommentProductModel> initIndex() {
        return dataset;
    }

    @Override
    public void processCommentProduct(String output) {
        Log.d("API comment_products", output);
        dataset.clear();
        int count = 0;
        try {
            JSONArray jsonArray = new JSONArray(output);
            float sumRating = 0;

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject mainObject = (JSONObject) jsonArray.get(i);
                String username = mainObject.getJSONObject("user").getString("name");
                String message = mainObject.getString("message");
                String date = mainObject.getString("date");
                int rating = mainObject.getInt("rating");
                int userIdCommentProduct = mainObject.getJSONObject("user").getInt("id");
//                int ownerProductId = mainObject.getJSONObject("product").getJSONObject("shop").getInt("user_id");
                int commentProductId = mainObject.getInt("id");

                if (userIdProduct != userIdCommentProduct) {
                    sumRating += rating;
                    count++;
                }

                Log.d("COMMENT PRODUCT", "user_id_product: " + String.valueOf(userIdProduct)
                        + ", user_id_comment: " + userIdCommentProduct + ", user_id_logged: " + userIdLogged);

                CommentProductModel item =
                        new CommentProductModel(message, rating, date, username, commentProductId, userIdCommentProduct, userIdProduct);
                dataset.add(item);
            }

            averageRating = sumRating / count;
            ratingBar.setRating(averageRating);
            Log.d("Average rating product", String.valueOf(averageRating));
            mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mAdapter = new CommentProductAdapter(getActivity(), initIndex(), getActivity());
            mRecyclerView.setAdapter(mAdapter);


        } catch (Exception e) {

        }
    }

    @Override
    public void processPost(String output) {
        new CommentProductGet(this, "/comment_products?product_id=" + productId + ".json").execute();
    }
}
