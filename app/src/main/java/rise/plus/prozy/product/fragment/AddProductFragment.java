package rise.plus.prozy.product.fragment;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import rise.plus.prozy.R;
import rise.plus.prozy.services.CategoryProductGet;
import rise.plus.prozy.services.ShopGet;
import rise.plus.prozy.services.async_response.CategoryProductResponse;
import rise.plus.prozy.services.async_response.ShopResponse;

import static rise.plus.prozy.Constant.UID;
import static rise.plus.prozy.Constant.userIdLogged;

public class AddProductFragment extends Fragment implements
        View.OnClickListener, AdapterView.OnItemSelectedListener, CategoryProductResponse, ShopResponse {
    List<String> categories = new ArrayList<String>();
    private Spinner spinner;
    private int categoryId, shopId;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_product, container, false);

        if (UID != null) {
            userIdLogged = Integer.parseInt(UID);
        }

        spinner = (Spinner) view.findViewById(R.id.category_spinner);
        Button createButton = (Button) view.findViewById(R.id.btn_create_product);
        createButton.setOnClickListener(this);

        new CategoryProductGet(this, "/category_products.json").execute();
        new ShopGet(this, "/shops.json?user_id=" + userIdLogged).execute();

        return view;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_create_product:
                create();
                break;
        }
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        categoryId = spinner.getSelectedItemPosition() + 1;
        Log.d("category-id", String.valueOf(categoryId));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void create() {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_create_product, null);
        builder.setView(view);

        final EditText edtProductName = (EditText) view.findViewById(R.id.product_name);
        final EditText edtPrice = (EditText) view.findViewById(R.id.product_price);

        builder.setPositiveButton("Create", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = edtProductName.getText().toString();
                float price = Float.valueOf(edtPrice.getText().toString());
                String image = "blank_image";
                Log.d("test", " name: " + name + ", price: " + price + ", category_product_id: " + categoryId);

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name", name);
                    jsonObject.put("price", price);
                    jsonObject.put("category_product_id", categoryId);
                    jsonObject.put("shop_id", shopId);
                    jsonObject.put("user_id", userIdLogged);
                    jsonObject.put("image", image);
                    Log.d("Post_product", String.valueOf(userIdLogged));

//                    new ServicePost(this, "/products.json", String.valueOf(jsonObject)).execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    @Override
    public void processCategoryProduct(String output) {

        try {
            JSONArray jsonArray = new JSONArray(output);
            Log.d("API category_products", output);

            spinner.setOnItemSelectedListener(this);

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                String name = jsonObject.getString("name");

                categories.add(name);

            }

            ArrayAdapter<String> dataAdapter =
                    new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapter);

        } catch (JSONException e) {
            Log.d("status process", "Error");
            e.printStackTrace();
        }
    }

    @Override
    public void processShop(String output) {
        Log.d("API shop", output);

        try {
            JSONObject jsonObject = new JSONObject(output);
            shopId = jsonObject.getInt("id");

        } catch (Exception e) {

        }
    }
}
